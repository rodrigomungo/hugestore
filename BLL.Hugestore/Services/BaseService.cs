﻿using System;
using System.Transactions;

namespace BLL.Hugestore.Services
{
    public abstract class BaseService
    {
        protected void SurroundTransaction(Action action)
        {
            using (TransactionScope trx = new TransactionScope())
            {
                try
                {
                    action();
                    trx.Complete();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        protected void SurroundTransaction(Action action, TransactionOptions options)
        {
            using (TransactionScope trx = new TransactionScope(TransactionScopeOption.Required, options))
            {
                try
                {
                    action();
                    trx.Complete();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        protected T SurroundTransaction<T>(Func<T> action)
        {
            using (TransactionScope trx = new TransactionScope())
            {
                try
                {
                    var result = action();
                    trx.Complete();
                    return result;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        protected T SurroundTransaction<T>(Func<T> action, TransactionOptions options)
        {
            using (TransactionScope trx = new TransactionScope(TransactionScopeOption.Required, options))
            {
                try
                {
                    var result = action();
                    trx.Complete();
                    return result;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
