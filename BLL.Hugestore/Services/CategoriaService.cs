﻿using System;
using System.Collections.Generic;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Factories;
using Domain.Hugestore;
using SL.Hugestore.Services;

namespace BLL.Hugestore.Services
{
    public sealed class CategoriaService
    {
        private IRepository<Categoria, int> _categoriasDao = Factory.Current.GetCategoriaRepository();

        #region Singleton
        private readonly static CategoriaService _instance = new CategoriaService();

        public static CategoriaService Current
        {
            get
            {
                return _instance;
            }
        }

        private CategoriaService() { }
        #endregion

        public void Actualizar(Categoria categoria)
        {
            try
            {
                _categoriasDao.Update(categoria);
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public void Agregar(Categoria categoria)
        {
            try
            {
                _categoriasDao.Insert(categoria);
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Categoria> Listar()
        {
            List<Categoria> categorias = new List<Categoria>();
            try
            {
                categorias = _categoriasDao.List();
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return categorias;
        }

        public Categoria ObtenerUno(int id)
        {
            Categoria categoria = default;
            try
            {
                categoria = _categoriasDao.Find(id);
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return categoria;
        }
    }
}
