﻿using System;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Factories;
using Domain.Hugestore;
using SL.Hugestore.Services;

namespace BLL.Hugestore.Services
{
    public sealed class EmpresaService
    {
        private IRepository<Empresa, long> _empresasDao = Factory.Current.GetEmpresasRepository();

        #region Singleton
        private readonly static EmpresaService _instance = new EmpresaService();

        public static EmpresaService Current
        {
            get
            {
                return _instance;
            }
        }

        private EmpresaService() { }
        #endregion

        public void Actualizar(Empresa empresa)
        {
            _empresasDao.Update(empresa);
        }

        public void Agregar(Empresa empresa)
        {
            _empresasDao.Insert(empresa);
        }

        public Empresa ObtenerUno(long aplicacionId)
        {
            Empresa empresa = default;
            try
            {
                empresa = _empresasDao.Find(aplicacionId);
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return empresa;
        }
    }
}
