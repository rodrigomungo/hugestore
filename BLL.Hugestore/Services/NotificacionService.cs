﻿using System;
using System.Collections.Generic;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Factories;
using Domain.Hugestore;
using SL.Hugestore.Services;

namespace BLL.Hugestore.Services
{
    public sealed class NotificacionService
    {
        private IRepository<Notificacion, Guid> _notificacionesDao = Factory.Current.GetNotificacionesRepository();

        #region Singleton
        private readonly static NotificacionService _instance = new NotificacionService();

        public static NotificacionService Current
        {
            get
            {
                return _instance;
            }
        }

        private NotificacionService() { }
        #endregion

        public void Agregar(Notificacion notificacion)
        {
            _notificacionesDao.Insert(notificacion);
        }

        public List<Notificacion> Listar()
        {
            List<Notificacion> notificaciones = new List<Notificacion>();
            try
            {
                notificaciones = _notificacionesDao.List();
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return notificaciones;
        }
    }
}
