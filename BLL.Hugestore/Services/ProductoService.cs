﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Factories;
using Domain.Hugestore;
using SL.Hugestore.Services;
using SL.Hugestore.Services.Extensions;

namespace BLL.Hugestore.Services
{
    public sealed class ProductoService : BaseService
    {
        private IRepository<Producto, Guid> _productosDao = Factory.Current.GetProductosRepository();
        private IRepository<Imagen, Guid> _imagenesDao = Factory.Current.GetImagenesRepository();
        private IRepository<Publicacion, Guid> _publicacionesDao = Factory.Current.GetPublicacionesRepository();
        private IRepository<AtributoEspecial, Guid> _atributosDao = Factory.Current.GetAtributosRepository();

        #region Singleton
        private readonly static ProductoService _instance = new ProductoService();

        public static ProductoService Current
        {
            get
            {
                return _instance;
            }
        }

        private ProductoService() { }
        #endregion

        public void Actualizar(Producto producto)
        {
            TransactionOptions options = new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };
            SurroundTransaction(() =>
            {
                _productosDao.Update(producto);
                _guardarImagenes(producto.Imagenes);
                _guardarAtributos(producto.Atributos);
                _guardarPublicaciones(producto.Publicaciones);
            }, options);
        }

        public void Agregar(Producto producto)
        {
            TransactionOptions options = new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };
            SurroundTransaction(() =>
            {
                _productosDao.Insert(producto);
                _guardarImagenes(producto.Imagenes);
                _guardarAtributos(producto.Atributos);
                _guardarPublicaciones(producto.Publicaciones);
            }, options);
        }

        public Producto ObtenerUno(Guid id)
        {
            Producto producto = default;
            try
            {
                producto = _productosDao.Find(id);
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return producto;
        }

        public List<Producto> Listar(Producto producto, int limit, int offset)
        {
            List<Producto> productos = new List<Producto>();
            try
            {
                productos = _productosDao.List();
                if (!producto.CategoriaId.Equals(0))
                {
                    productos = productos.Where(p => p.CategoriaId == producto.CategoriaId).ToList();
                }

                if (!producto.SKU.IsNullOrEmpty() || !producto.Nombre.IsNullOrEmpty())
                {
                    productos = productos.
                        Where(p => p.Nombre.Contains(producto.Nombre) || p.SKU.Contains(producto.SKU)).
                        ToList();
                }

                productos = productos.Skip(offset).Take(limit).ToList();

                // TODO: Include images, attributes and publications
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return productos;
        }

        private void _guardarImagenes(List<Imagen> imagenes)
        {
            foreach(Imagen imagen in imagenes)
            {
                if (imagen.Id == null)
                {
                    _imagenesDao.Insert(imagen);
                }
                else
                {
                    _imagenesDao.Update(imagen);
                }
            }
        }

        private void _guardarAtributos(List<AtributoEspecial> atributos)
        {
            foreach (AtributoEspecial atributo in atributos)
            {
                if (atributo.Id == null)
                {
                    _atributosDao.Insert(atributo);
                }
                else
                {
                    _atributosDao.Update(atributo);
                }
            }
        }

        private void _guardarPublicaciones(List<Publicacion> publicaciones)
        {
            foreach (Publicacion publicacion in publicaciones)
            {
                if (publicacion.Id == null)
                {
                    _publicacionesDao.Insert(publicacion);
                }
                else
                {
                    _publicacionesDao.Update(publicacion);
                }
            }
        }
    }
}
