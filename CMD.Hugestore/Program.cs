﻿using System;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services.Security;

namespace CMD.Hugestore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido a la creación de usuarios");
            Console.Write("Ingrese su nombre: ");
            string nombre = Console.ReadLine();

            Console.Write("Ingrese su apellido: ");
            string apellido = Console.ReadLine();

            Console.Write("Ingrese su nombre de usuario: ");
            string nombreUsuario = Console.ReadLine();

            Console.Write("Ingrese su contraseña: ");
            string clave = Console.ReadLine();

            Usuario usuario = new Usuario()
            {
                Nombre = nombre,
                Apellido = apellido,
                NombreUsuario = nombreUsuario,
                Clave = clave
            };

            UserFacade.Current.Register(usuario);
            Console.WriteLine("Usuario creado con éxito");
            Console.ReadKey();
        }
    }
}
