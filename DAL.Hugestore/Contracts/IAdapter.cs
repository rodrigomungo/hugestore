﻿namespace DAL.Hugestore.Contracts
{
    internal interface IAdapter<T>
    {
        T Adapt(object[] values);
    }
}
