﻿using System.Collections.Generic;

namespace DAL.Hugestore.Contracts
{
    public interface IRelationship<T, U>
    {
        void Join(T parent);
        List<U> Get(T parent);
    }
}
