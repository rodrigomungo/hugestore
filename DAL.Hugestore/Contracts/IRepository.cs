﻿using System.Collections.Generic;

namespace DAL.Hugestore.Contracts
{
    public interface IRepository<T, U>
    {
        void Delete(U id);
        T Find(U id);
        void Insert(T entity);
        List<T> List();
        void Update(T entity);
    }
}
