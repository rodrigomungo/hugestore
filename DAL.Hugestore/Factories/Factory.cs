﻿using System;
using Domain.Hugestore;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer;

namespace DAL.Hugestore.Factories
{
    public sealed class Factory
    {
        #region Singleton
        private readonly static Factory _instance = new Factory();

        public static Factory Current
        {
            get
            {
                return _instance;
            }
        }

        private Factory() { }
        #endregion

        public IRepository<Producto, Guid> GetProductosRepository()
        {
            return new ProductosRepository();
        }

        public IRepository<Imagen, Guid> GetImagenesRepository()
        {
            return new ImagenesRepository();
        }

        public IRepository<Categoria, int> GetCategoriaRepository()
        {
            return new CategoriasRepository();
        }

        public IRepository<AtributoEspecial, Guid> GetAtributosRepository()
        {
            return new AtributosEspecialesRepository();
        }

        public IRepository<Publicacion, Guid> GetPublicacionesRepository()
        {
            return new PublicacionesRepository();
        }

        public IRepository<Empresa, long> GetEmpresasRepository()
        {
            return new EmpresasRepository();
        }

        public IRepository<Notificacion, Guid> GetNotificacionesRepository()
        {
            return new NotificacionesRepository();
        }
    }
}
