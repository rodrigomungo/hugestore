﻿using System;
using Domain.Hugestore;
using DAL.Hugestore.Contracts;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class AtributoEspecialAdapter : IAdapter<AtributoEspecial>
    {
        #region Singleton
        private readonly static AtributoEspecialAdapter _instance = new AtributoEspecialAdapter();

        public static AtributoEspecialAdapter Current
        {
            get => _instance;
        }

        private AtributoEspecialAdapter() { }
        #endregion

        public AtributoEspecial Adapt(object[] values) => new AtributoEspecial()
        {
            Id = Guid.Parse(values[(int)Columns.ID].ToString()),
            Nombre = values[(int)Columns.NOMBRE].ToString(),
            Tipo = (TipoValor)int.Parse(values[(int)Columns.TIPO_VALOR].ToString()),
            Valor = values[(int)Columns.VALOR].ToString(),
            ProductoId = Guid.Parse(values[(int)Columns.PRODUCTO_ID].ToString())
        };

        private enum Columns
        {
            ID,
            NOMBRE,
            TIPO_VALOR,
            VALOR,
            PRODUCTO_ID
        }
    }
}
