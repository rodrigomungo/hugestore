﻿using Domain.Hugestore;
using DAL.Hugestore.Contracts;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class CategoriaAdapter : IAdapter<Categoria>
    {
        #region Singleton
        private readonly static CategoriaAdapter _instance = new CategoriaAdapter();

        public static CategoriaAdapter Current
        {
            get => _instance;
        }

        private CategoriaAdapter() { }
        #endregion

        public Categoria Adapt(object[] values) => new Categoria()
        {
            Id = int.Parse(values[(int)Columns.ID].ToString()),
            Nombre = values[(int)Columns.NOMBRE].ToString(),
            Descripcion = values[(int)Columns.DESCRIPCION].ToString()
        };

        private enum Columns
        {
            ID,
            NOMBRE,
            DESCRIPCION,
        }
    }
}
