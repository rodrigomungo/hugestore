﻿using System;
using Domain.Hugestore;
using DAL.Hugestore.Contracts;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class EmpresaAdapter : IAdapter<Empresa>
    {
        #region Singleton
        private readonly static EmpresaAdapter _instance = new EmpresaAdapter();

        public static EmpresaAdapter Current
        {
            get => _instance;
        }

        private EmpresaAdapter() { }
        #endregion

        public Empresa Adapt(object[] values)
        {
            Empresa empresa = new Empresa()
            {
                Id = int.Parse(values[(int)Columns.ID].ToString()),
                Cuit = values[(int)Columns.CUIT].ToString(),
                RazonSocial = values[(int)Columns.RAZON_SOCIAL].ToString(),
                AplicacionId = long.Parse(values[(int)Columns.APLICACION_ID].ToString()),
                ClaveSecreta = values[(int)Columns.CLAVE_SECRETA].ToString(),
                Token = values[(int)Columns.TOKEN].ToString(),
                TokenRefresco = values[(int)Columns.TOKEN_REFRESCO].ToString(),
                Expiracion = long.Parse(values[(int)Columns.EXPIRACION].ToString()),
            };

            DateTimeOffset date;
            DateTimeOffset.TryParse(values[(int)Columns.FECHA_AUTENTICACION].ToString(), out date);
            empresa.FechaAutenticacion = date;
            return empresa;
        }

        private enum Columns
        {
            ID,
            CUIT,
            RAZON_SOCIAL,
            APLICACION_ID,
            CLAVE_SECRETA,
            TOKEN,
            TOKEN_REFRESCO,
            FECHA_AUTENTICACION,
            EXPIRACION,
            CERRADO
        }
    }
}
