﻿using System;
using Domain.Hugestore;
using DAL.Hugestore.Contracts;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class ImagenAdapter : IAdapter<Imagen>
    {
        #region Singleton
        private readonly static ImagenAdapter _instance = new ImagenAdapter();

        public static ImagenAdapter Current
        {
            get => _instance;
        }

        private ImagenAdapter() { }
        #endregion

        public Imagen Adapt(object[] values) => new Imagen()
        {
            Id = Guid.Parse(values[(int)Columns.ID].ToString()),
            Url = values[(int)Columns.URL].ToString(),
            SecureUrl = values[(int)Columns.SECURE_URL].ToString(),
            OriginUrl = values[(int)Columns.ORIGIN_URL].ToString(),
            OriginSecureUrl = values[(int)Columns.ORIGIN_SECURE_URL].ToString(),
            ProductoId = Guid.Parse(values[(int)Columns.PRODUCTO_ID].ToString()),
        };

        private enum Columns
        {
            ID,
            URL,
            SECURE_URL,
            ORIGIN_URL,
            ORIGIN_SECURE_URL,
            PRODUCTO_ID
        }
    }
}
