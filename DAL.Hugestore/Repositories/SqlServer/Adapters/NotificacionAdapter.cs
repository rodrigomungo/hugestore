﻿using System;
using DAL.Hugestore.Contracts;
using Domain.Hugestore;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class NotificacionAdapter : IAdapter<Notificacion>
    {
        #region Singleton
        private readonly static NotificacionAdapter _instance = new NotificacionAdapter();

        public static NotificacionAdapter Current
        {
            get => _instance;
        }

        private NotificacionAdapter() { }
        #endregion

        public Notificacion Adapt(object[] values) => new Notificacion()
        {
            Id = Guid.Parse(values[(int)Columns.ID].ToString()),
            Recurso = values[(int)Columns.RECURSO].ToString(),
            Topico = values[(int)Columns.TOPICO].ToString(),
            FechaCreacion = DateTimeOffset.Parse(values[(int)Columns.FECHA_CREACION].ToString())
        };

        private enum Columns
        {
            ID,
            RECURSO,
            TOPICO,
            FECHA_CREACION
        }
    }
}
