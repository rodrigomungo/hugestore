﻿using System;
using Domain.Hugestore;
using DAL.Hugestore.Contracts;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class ProductoAdapter : IAdapter<Producto>
    {
        #region Singleton
        private readonly static ProductoAdapter _instance = new ProductoAdapter();

        public static ProductoAdapter Current
        {
            get => _instance;
        }

        private ProductoAdapter() { }
        #endregion

        public Producto Adapt(object[] values) => new Producto()
        {
            Id = Guid.Parse(values[(int)Columns.ID].ToString()),
            Nombre = values[(int)Columns.NOMBRE].ToString(),
            Descripcion = values[(int)Columns.DESCRIPCION].ToString(),
            SKU = values[(int)Columns.SKU].ToString(),
            PrecioBase = decimal.Parse(values[(int)Columns.PRECIO_BASE].ToString()),
            Precio = decimal.Parse(values[(int)Columns.PRECIO].ToString()),
            CategoriaId = int.Parse(values[(int)Columns.CATEGORIA_ID].ToString()),
        };

        private enum Columns
        {
            ID,
            NOMBRE,
            DESCRIPCION,
            SKU,
            PRECIO_BASE,
            PRECIO,
            CATEGORIA_ID
        }
    }
}
