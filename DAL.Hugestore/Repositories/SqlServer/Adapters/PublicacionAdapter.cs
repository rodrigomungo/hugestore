﻿using Domain.Hugestore;
using DAL.Hugestore.Contracts;
using System;

namespace DAL.Hugestore.Repositories.SqlServer.Adapters
{
    public sealed class PublicacionAdapter : IAdapter<Publicacion>
    {
        #region Singleton
        private readonly static PublicacionAdapter _instance = new PublicacionAdapter();

        public static PublicacionAdapter Current
        {
            get => _instance;
        }

        private PublicacionAdapter() { }
        #endregion

        public Publicacion Adapt(object[] values) => new Publicacion()
        {
            Id = Guid.Parse(values[(int)Columns.ID].ToString()),
            PublicacionId = values[(int)Columns.PUBLICACION_ID].ToString(),
            Estado = values[(int)Columns.ESTADO].ToString(),
            Precio = decimal.Parse(values[(int)Columns.PRECIO].ToString()),
            Titulo = values[(int)Columns.TITULO].ToString(),
            Descripcion = values[(int)Columns.DESCRIPCION].ToString(),
            ProductoId = Guid.Parse(values[(int)Columns.PRODUCTO_ID].ToString()),
        };

        private enum Columns
        {
            ID,
            PUBLICACION_ID,
            ESTADO,
            PRECIO,
            TITULO,
            DESCRIPCION,
            PRODUCTO_ID
        }
    }
}
