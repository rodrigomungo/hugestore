﻿using System;
using System.Collections.Generic;
using Domain.Hugestore;
using System.Data.SqlClient;
using SL.Hugestore.Services;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class AtributosEspecialesRepository : IRepository<AtributoEspecial, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[AtributosEspeciales] (TipoValor, Nombre, Valor, ProductoId) OUTPUT Inserted.Id VALUES (@TipoValor, @Nombre, @Valor, @ProductoId)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[AtributosEspeciales] SET TipoValor = @TipoValor, Nombre = @Nombre, Valor = @Valor, ProductoId = @ProductoId WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "DELETE [dbo].[AtributosEspeciales] WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, TipoValor, Nombre, Valor, ProductoId FROM [dbo].[AtributosEspeciales] WHERE Id = @Id";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, TipoValor, Nombre, Valor, ProductoId FROM [dbo].[AtributosEspeciales]";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public AtributoEspecial Find(Guid id)
        {
            AtributoEspecial atributo = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        atributo = AtributoEspecialAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return atributo;
        }

        public void Insert(AtributoEspecial entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@TipoValor", entity.Tipo),
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Valor", entity.Valor),
                    new SqlParameter("@ProductoId", entity.ProductoId));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<AtributoEspecial> List()
        {
            List<AtributoEspecial> atributos = new List<AtributoEspecial>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        atributos.Add(AtributoEspecialAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return atributos;
        }

        public void Update(AtributoEspecial entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@TipoValor", entity.Tipo),
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Valor", entity.Valor),
                    new SqlParameter("@ProductoId", entity.ProductoId),
                    new SqlParameter("@Id", entity.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
