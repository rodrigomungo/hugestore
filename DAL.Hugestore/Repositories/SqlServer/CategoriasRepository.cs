﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Domain.Hugestore;
using SL.Hugestore.Services;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class CategoriasRepository : IRepository<Categoria, int>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Categorias] (Nombre, Descripcion) OUTPUT Inserted.Id VALUES (@Nombre, @Descripcion)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Categorias] SET Nombre = @Nombre, Descripcion = @Descripcion WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "DELETE [dbo].[Categorias] WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Nombre, Descripcion FROM [dbo].[Categorias] WHERE Id = @Id";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Nombre, Descripcion FROM [dbo].[Categorias]";
        }
        #endregion

        public void Delete(int id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public Categoria Find(int id)
        {
            Categoria categoria = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        categoria = CategoriaAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return categoria;
        }

        public void Insert(Categoria entity)
        {
            try
            {
                entity.Id = (int)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Descripcion", entity.Descripcion));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Categoria> List()
        {
            List<Categoria> categorias = new List<Categoria>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        categorias.Add(CategoriaAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return categorias;
        }

        public void Update(Categoria entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Descripcion", entity.Descripcion),
                    new SqlParameter("@Id", entity.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
