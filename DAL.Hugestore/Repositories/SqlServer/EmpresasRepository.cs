﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;
using Domain.Hugestore;
using SL.Hugestore.Services;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class EmpresasRepository : IRepository<Empresa, long>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Empresas] (Cuit, RazonSocial, AplicacionId, ClaveSecreta) OUTPUT Inserted.Id VALUES (@Cuit, @RazonSocial, @AplicacionId, @ClaveSecreta)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Empresas] SET Token = @Token, TokenRefresco = @TokenRefresco, FechaAutenticacion = @FechaAutenticacion, Expiracion = @Expiracion WHERE AplicacionId = @AplicacionId";
        }

        private string DeleteStatement
        {
            get => "UPDATE [dbo].[Empresas] SET Cerrado = 1 WHERE AplicacionId = @AplicacionId";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Cuit, RazonSocial, AplicacionId, ClaveSecreta, Token, TokenRefresco, FechaAutenticacion, Expiracion FROM [dbo].[Empresas] WHERE AplicacionId = @AplicacionId";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Cuit, RazonSocial, AplicacionId, ClaveSecreta, Token, TokenRefresco, FechaAutenticacion, Expiracion FROM [dbo].[Empresas]";
        }
        #endregion

        public void Delete(long aplicacionId)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@AplicacionId", aplicacionId));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public Empresa Find(long aplicacionId)
        {
            Empresa empresa = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@AplicacionId", aplicacionId)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        empresa = EmpresaAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return empresa;
        }

        public void Insert(Empresa entity)
        {
            try
            {
                entity.Id = (int)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Cuit", entity.Cuit),
                    new SqlParameter("@RazonSocial", entity.RazonSocial),
                    new SqlParameter("@AplicacionId", entity.AplicacionId),
                    new SqlParameter("@ClaveSecreta", entity.ClaveSecreta));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Empresa> List()
        {
            List<Empresa> empresas = new List<Empresa>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        empresas.Add(EmpresaAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return empresas;
        }

        public void Update(Empresa entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Token", entity.Token),
                    new SqlParameter("@TokenRefresco", entity.TokenRefresco),
                    new SqlParameter("@FechaAutenticacion", DateTimeOffset.UtcNow),
                    new SqlParameter("@Expiracion", entity.Expiracion),
                    new SqlParameter("@AplicacionId", entity.AplicacionId));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
