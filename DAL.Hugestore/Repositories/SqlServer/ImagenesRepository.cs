﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Domain.Hugestore;
using SL.Hugestore.Services;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class ImagenesRepository : IRepository<Imagen, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Imagenes] (Url, SecureUrl, OriginUrl, OriginSecureUrl, ProductoId) OUTPUT Inserted.Id VALUES (@Url, @SecureUrl, @OriginUrl, @OriginSecureUrl, @ProductoId)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Imagenes] SET Url = @Url, SecureUrl = @SecureUrl, OriginUrl = @OriginUrl, OriginSecureUrl = @OriginSecureUrl, ProductoId = @ProductoId WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "DELETE [dbo].[Imagenes] WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Url, SecureUrl, OriginUrl, OriginSecureUrl, ProductoId FROM [dbo].[Imagenes] WHERE Id = @Id";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Url, SecureUrl, OriginUrl, OriginSecureUrl, ProductoId FROM [dbo].[Imagenes]";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public Imagen Find(Guid id)
        {
            Imagen imagen = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        imagen = ImagenAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return imagen;
        }

        public void Insert(Imagen entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Url", entity.Url),
                    new SqlParameter("@SecureUrl", entity.SecureUrl),
                    new SqlParameter("@OriginUrl", entity.OriginUrl),
                    new SqlParameter("@OriginSecureUrl", entity.OriginSecureUrl),
                    new SqlParameter("@ProductoId", entity.ProductoId));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Imagen> List()
        {
            List<Imagen> imagenes = new List<Imagen>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        imagenes.Add(ImagenAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return imagenes;
        }

        public void Update(Imagen entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Url", entity.Url),
                    new SqlParameter("@SecureUrl", entity.SecureUrl),
                    new SqlParameter("@OriginUrl", entity.OriginUrl),
                    new SqlParameter("@OriginSecureUrl", entity.OriginSecureUrl),
                    new SqlParameter("@ProductoId", entity.ProductoId),
                    new SqlParameter("@Id", entity.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
