﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;
using Domain.Hugestore;
using SL.Hugestore.Services;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class NotificacionesRepository : IRepository<Notificacion, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Notificaciones] (Recurso, Topico) OUTPUT Inserted.Id VALUES (@Recurso, @Topico)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Notificaciones] SET Recurso = @Recurso, Topico = @Topico WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "UPDATE [dbo].[Notificaciones] SET FechaCierre = GETDATE() WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Recurso, Topico, FechaCreacion FROM [dbo].[Notificaciones] WHERE Id = @Id AND FechaCierre IS NULL";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Recurso, Topico, FechaCreacion FROM [dbo].[Notificaciones] WHERE FechaCierre IS NULL";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public Notificacion Find(Guid id)
        {
            Notificacion notificacion = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        notificacion = NotificacionAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return notificacion;
        }

        public void Insert(Notificacion entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Recurso", entity.Recurso),
                    new SqlParameter("@Topico", entity.Topico));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Notificacion> List()
        {
            List<Notificacion> notificaciones = new List<Notificacion>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        notificaciones.Add(NotificacionAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return notificaciones;
        }

        public void Update(Notificacion entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Recurso", entity.Recurso),
                    new SqlParameter("@Topico", entity.Topico));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
