﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Domain.Hugestore;
using SL.Hugestore.Services;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class ProductosRepository : IRepository<Producto, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Productos] (Nombre, Descripcion, SKU, PrecioBase, Precio, CategoriaId) OUTPUT Inserted.Id VALUES (@Nombre, @Descripcion, @SKU, @PrecioBase, @Precio, @CategoriaId)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Productos] SET Nombre = @Nombre, Descripcion = @Descripcion, SKU = @SKU, PrecioBase = @PrecioBase, Precio = @Precio, CategoriaId = @CategoriaId WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "UPDATE [dbo].[Productos] SET FechaCierre = GETDATE() WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Nombre, Descripcion, SKU, PrecioBase, Precio, CategoriaId FROM [dbo].[Productos] WHERE Id = @Id AND FechaCierre IS NULL";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Nombre, Descripcion, SKU, PrecioBase, Precio, CategoriaId FROM [dbo].[Productos] WHERE FechaCierre IS NULL";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public Producto Find(Guid id)
        {
            Producto producto = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        producto = ProductoAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return producto;
        }

        public void Insert(Producto entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Descripcion", entity.Descripcion),
                    new SqlParameter("@SKU", entity.SKU),
                    new SqlParameter("@PrecioBase", entity.PrecioBase),
                    new SqlParameter("@Precio", entity.Precio),
                    new SqlParameter("@CategoriaId", entity.CategoriaId));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Producto> List()
        {
            List<Producto> productos = new List<Producto>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        productos.Add(ProductoAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return productos;
        }

        public void Update(Producto entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Descripcion", entity.Descripcion),
                    new SqlParameter("@SKU", entity.SKU),
                    new SqlParameter("@PrecioBase", entity.PrecioBase),
                    new SqlParameter("@Precio", entity.Precio),
                    new SqlParameter("@CategoriaId", entity.CategoriaId),
                    new SqlParameter("@Id", entity.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
