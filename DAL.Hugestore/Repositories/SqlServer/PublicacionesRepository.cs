﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Domain.Hugestore;
using SL.Hugestore.Services;
using DAL.Hugestore.Contracts;
using DAL.Hugestore.Repositories.SqlServer.Adapters;
using DAL.Hugestore.Tools;

namespace DAL.Hugestore.Repositories.SqlServer
{
    internal sealed class PublicacionesRepository : IRepository<Publicacion, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Publicaciones] (PublicacionId, Estado, Precio, Titulo, Descripcion, ProductoId) OUTPUT Inserted.Id VALUES (@PublicacionId, @Estado, @Precio, @Titulo, @Descripcion, @ProductoId)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Publicaciones] SET PublicacionId = @PublicacionId, Estado = @Estado, Precio = @Precio, Titulo = @Titulo, Descripcion = @Descripcion, ProductoId = @ProductoId WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "DELETE [dbo].[Publicaciones] WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, PublicacionId, Estado, Precio, Titulo, Descripcion, ProductoId FROM [dbo].[Publicaciones] WHERE Id = @Id";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, PublicacionId, Estado, Precio, Titulo, Descripcion, ProductoId FROM [dbo].[Publicaciones]";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public Publicacion Find(Guid id)
        {
            Publicacion publicacion = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        publicacion = PublicacionAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return publicacion;
        }

        public void Insert(Publicacion entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@PublicacionId", entity.PublicacionId),
                    new SqlParameter("@Estado", entity.Estado),
                    new SqlParameter("@Precio", entity.Precio),
                    new SqlParameter("@Titulo", entity.Titulo),
                    new SqlParameter("@Descripcion", entity.Descripcion),
                    new SqlParameter("@ProductoId", entity.ProductoId));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public List<Publicacion> List()
        {
            List<Publicacion> publicaciones = new List<Publicacion>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        publicaciones.Add(PublicacionAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return publicaciones;
        }

        public void Update(Publicacion entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@PublicacionId", entity.PublicacionId),
                    new SqlParameter("@Estado", entity.Estado),
                    new SqlParameter("@Precio", entity.Precio),
                    new SqlParameter("@Titulo", entity.Titulo),
                    new SqlParameter("@Descripcion", entity.Descripcion),
                    new SqlParameter("@ProductoId", entity.ProductoId),
                    new SqlParameter("@Id", entity.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
