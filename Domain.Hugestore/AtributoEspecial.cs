﻿using System;

namespace Domain.Hugestore
{
    public class AtributoEspecial
    {
        public Guid Id { get; set; }
        public TipoValor Tipo { get; set; }
        public string Nombre { get; set; }
        public string Valor { get; set; }
        public Guid ProductoId { get; set; }

        public Producto Producto { get; set; }
    }

    public enum TipoValor : int
    {
        STRING = 0,
        INT = 1,
        DECIMAL = 2,
        BOOL = 3,
        LONG = 4
    }
}
