﻿using System;

namespace Domain.Hugestore
{
    public class Empresa
    {
        public int Id { get; set; }
        public string Cuit { get; set; }
        public string RazonSocial { get; set; }
        public long AplicacionId { get; set; }
        public string ClaveSecreta { get; set; }
        public string Token { get; set; }
        public string TokenRefresco { get; set; }
        public DateTimeOffset FechaAutenticacion { get; set; }
        public long Expiracion { get; set; }
        public bool Cerrado { get; set; }
    }
}
