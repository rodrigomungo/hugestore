﻿using System;

namespace Domain.Hugestore
{
    public class Imagen
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string SecureUrl { get; set; }
        public string OriginUrl { get; set; }
        public string OriginSecureUrl { get; set; }
        public Guid ProductoId { get; set; }

        public Producto Producto { get; set; }
    }
}
