﻿using System;

namespace Domain.Hugestore
{
    public class Notificacion
    {
        public Guid Id { get; set; }
        public string Recurso { get; set; }
        public string Topico { get; set; }
        public DateTimeOffset FechaCreacion { get; set; }
        public DateTimeOffset FechaCierre { get; set; }
    }
}
