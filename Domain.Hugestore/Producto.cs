﻿using System;
using System.Collections.Generic;

namespace Domain.Hugestore
{
    public class Producto
    {
        public Guid Id { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public DateTime FechaCierre { get; set; }
        public string SKU { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public decimal PrecioBase { get; set; }
        public decimal Precio { get; set; }
        public int CategoriaId { get; set; }

        public Categoria Categoria { get; set; } = new Categoria();
        public List<Imagen> Imagenes { get; set; } = new List<Imagen>();
        public List<Publicacion> Publicaciones { get; set; } = new List<Publicacion>();
        public List<AtributoEspecial> Atributos { get; set; } = new List<AtributoEspecial>();
    }
}
