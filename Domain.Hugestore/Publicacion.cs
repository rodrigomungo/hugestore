﻿using System;

namespace Domain.Hugestore
{
    public class Publicacion
    {
        public Guid Id { get; set; }
        public string PublicacionId { get; set; }
        public string Estado { get; set; }
        public decimal Precio { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public Guid ProductoId { get; set; }

        public Producto Producto { get; set; }
    }
}
