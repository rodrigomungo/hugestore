﻿using System;
using System.Data;
using System.Windows.Forms;
using Hugestore.Controllers;
using Hugestore.ViewModels;

namespace Hugestore.BodyForms
{
    public partial class CategoriaForm : Form
    {
        public CategoriaForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaView view = new CategoriaView(EntityState.Added)
                {
                    Nombre = tbName.Text,
                    Descripcion = tbDescription.Text
                };
                CategoriaController.Current.SaveChanges(view);
                MessageBox.Show("La categoria fue guardada", "Hugestore", MessageBoxButtons.OK);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
