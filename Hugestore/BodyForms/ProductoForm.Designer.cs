﻿
using Hugestore.Controls;

namespace Hugestore.BodyForms
{
    partial class ProductoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpProduct = new System.Windows.Forms.TabPage();
            this.btnAddAlarm = new System.Windows.Forms.Button();
            this.lbDescription = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPrice = new Hugestore.Controls.MoneyTextBox();
            this.lbBasePrice = new System.Windows.Forms.Label();
            this.tbBasePrice = new Hugestore.Controls.MoneyTextBox();
            this.tpPublication = new System.Windows.Forms.TabPage();
            this.lbMLStatus = new System.Windows.Forms.Label();
            this.btnPublish = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbMLDescription = new System.Windows.Forms.Label();
            this.tbMLDescription = new System.Windows.Forms.TextBox();
            this.lbMLShippingType = new System.Windows.Forms.Label();
            this.cbMLShippingType = new System.Windows.Forms.ComboBox();
            this.lbMLWarranty = new System.Windows.Forms.Label();
            this.cbMLWarranty = new System.Windows.Forms.ComboBox();
            this.lbMLCondition = new System.Windows.Forms.Label();
            this.cbMLCondition = new System.Windows.Forms.ComboBox();
            this.lbMLDiscount = new System.Windows.Forms.Label();
            this.tbMLDiscount = new System.Windows.Forms.MaskedTextBox();
            this.lbMLPrice = new System.Windows.Forms.Label();
            this.tbMLPrice = new System.Windows.Forms.MaskedTextBox();
            this.lbTitle = new System.Windows.Forms.Label();
            this.tbMLTitle = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbSKU = new System.Windows.Forms.Label();
            this.tbSKU = new System.Windows.Forms.TextBox();
            this.lbNombre = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tpProduct.SuspendLayout();
            this.tpPublication.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpProduct);
            this.tabControl1.Controls.Add(this.tpPublication);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.tabControl1.Location = new System.Drawing.Point(13, 108);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(397, 411);
            this.tabControl1.TabIndex = 0;
            // 
            // tpProduct
            // 
            this.tpProduct.Controls.Add(this.btnAddAlarm);
            this.tpProduct.Controls.Add(this.lbDescription);
            this.tpProduct.Controls.Add(this.tbDescription);
            this.tpProduct.Controls.Add(this.label2);
            this.tpProduct.Controls.Add(this.tbPrice);
            this.tpProduct.Controls.Add(this.lbBasePrice);
            this.tpProduct.Controls.Add(this.tbBasePrice);
            this.tpProduct.Location = new System.Drawing.Point(4, 26);
            this.tpProduct.Name = "tpProduct";
            this.tpProduct.Padding = new System.Windows.Forms.Padding(3);
            this.tpProduct.Size = new System.Drawing.Size(389, 381);
            this.tpProduct.TabIndex = 0;
            this.tpProduct.Text = "Detalle";
            this.tpProduct.UseVisualStyleBackColor = true;
            // 
            // btnAddAlarm
            // 
            this.btnAddAlarm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(233)))), ((int)(((byte)(250)))));
            this.btnAddAlarm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddAlarm.FlatAppearance.BorderSize = 0;
            this.btnAddAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAlarm.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAlarm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(131)))), ((int)(((byte)(250)))));
            this.btnAddAlarm.Location = new System.Drawing.Point(304, 23);
            this.btnAddAlarm.Name = "btnAddAlarm";
            this.btnAddAlarm.Size = new System.Drawing.Size(79, 54);
            this.btnAddAlarm.TabIndex = 19;
            this.btnAddAlarm.Text = "CARGAR ALARMA";
            this.btnAddAlarm.UseVisualStyleBackColor = false;
            this.btnAddAlarm.Click += new System.EventHandler(this.btnAddAlarm_Click);
            // 
            // lbDescription
            // 
            this.lbDescription.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDescription.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbDescription.Location = new System.Drawing.Point(3, 61);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(295, 19);
            this.lbDescription.TabIndex = 18;
            this.lbDescription.Text = "Descripción";
            this.lbDescription.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbDescription
            // 
            this.tbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDescription.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDescription.Location = new System.Drawing.Point(6, 108);
            this.tbDescription.Multiline = true;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(377, 242);
            this.tbDescription.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label2.Location = new System.Drawing.Point(152, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Precio consumidor";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(155, 23);
            this.tbPrice.Mask = "000000000.00";
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(143, 25);
            this.tbPrice.TabIndex = 15;
            this.tbPrice.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.tbPrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // lbBasePrice
            // 
            this.lbBasePrice.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBasePrice.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbBasePrice.Location = new System.Drawing.Point(3, 3);
            this.lbBasePrice.Name = "lbBasePrice";
            this.lbBasePrice.Size = new System.Drawing.Size(143, 17);
            this.lbBasePrice.TabIndex = 14;
            this.lbBasePrice.Text = "Costo";
            this.lbBasePrice.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbBasePrice
            // 
            this.tbBasePrice.Location = new System.Drawing.Point(6, 23);
            this.tbBasePrice.Mask = "000000000.00";
            this.tbBasePrice.Name = "tbBasePrice";
            this.tbBasePrice.Size = new System.Drawing.Size(143, 25);
            this.tbBasePrice.TabIndex = 13;
            this.tbBasePrice.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            this.tbBasePrice.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // tpPublication
            // 
            this.tpPublication.Controls.Add(this.lbMLStatus);
            this.tpPublication.Controls.Add(this.btnPublish);
            this.tpPublication.Controls.Add(this.tabControl2);
            this.tpPublication.Location = new System.Drawing.Point(4, 26);
            this.tpPublication.Name = "tpPublication";
            this.tpPublication.Padding = new System.Windows.Forms.Padding(3);
            this.tpPublication.Size = new System.Drawing.Size(389, 381);
            this.tpPublication.TabIndex = 1;
            this.tpPublication.Text = "Publicación";
            this.tpPublication.UseVisualStyleBackColor = true;
            // 
            // lbMLStatus
            // 
            this.lbMLStatus.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLStatus.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLStatus.Location = new System.Drawing.Point(135, 350);
            this.lbMLStatus.Name = "lbMLStatus";
            this.lbMLStatus.Size = new System.Drawing.Size(247, 25);
            this.lbMLStatus.TabIndex = 31;
            this.lbMLStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnPublish
            // 
            this.btnPublish.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(200)))));
            this.btnPublish.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPublish.FlatAppearance.BorderSize = 0;
            this.btnPublish.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPublish.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPublish.ForeColor = System.Drawing.Color.White;
            this.btnPublish.Location = new System.Drawing.Point(6, 350);
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(123, 25);
            this.btnPublish.TabIndex = 15;
            this.btnPublish.Text = "PUBLICAR";
            this.btnPublish.UseVisualStyleBackColor = false;
            this.btnPublish.Click += new System.EventHandler(this.btnPublish_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(383, 339);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lbMLDescription);
            this.tabPage1.Controls.Add(this.tbMLDescription);
            this.tabPage1.Controls.Add(this.lbMLShippingType);
            this.tabPage1.Controls.Add(this.cbMLShippingType);
            this.tabPage1.Controls.Add(this.lbMLWarranty);
            this.tabPage1.Controls.Add(this.cbMLWarranty);
            this.tabPage1.Controls.Add(this.lbMLCondition);
            this.tabPage1.Controls.Add(this.cbMLCondition);
            this.tabPage1.Controls.Add(this.lbMLDiscount);
            this.tabPage1.Controls.Add(this.tbMLDiscount);
            this.tabPage1.Controls.Add(this.lbMLPrice);
            this.tabPage1.Controls.Add(this.tbMLPrice);
            this.tabPage1.Controls.Add(this.lbTitle);
            this.tabPage1.Controls.Add(this.tbMLTitle);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(375, 309);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Detalle";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lbMLDescription
            // 
            this.lbMLDescription.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLDescription.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLDescription.Location = new System.Drawing.Point(3, 167);
            this.lbMLDescription.Name = "lbMLDescription";
            this.lbMLDescription.Size = new System.Drawing.Size(366, 19);
            this.lbMLDescription.TabIndex = 42;
            this.lbMLDescription.Text = "Descripción";
            this.lbMLDescription.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbMLDescription
            // 
            this.tbMLDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMLDescription.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMLDescription.Location = new System.Drawing.Point(6, 192);
            this.tbMLDescription.Multiline = true;
            this.tbMLDescription.Name = "tbMLDescription";
            this.tbMLDescription.Size = new System.Drawing.Size(363, 107);
            this.tbMLDescription.TabIndex = 41;
            // 
            // lbMLShippingType
            // 
            this.lbMLShippingType.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLShippingType.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLShippingType.Location = new System.Drawing.Point(125, 115);
            this.lbMLShippingType.Name = "lbMLShippingType";
            this.lbMLShippingType.Size = new System.Drawing.Size(119, 17);
            this.lbMLShippingType.TabIndex = 40;
            this.lbMLShippingType.Text = "Método de envío";
            this.lbMLShippingType.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cbMLShippingType
            // 
            this.cbMLShippingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMLShippingType.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbMLShippingType.FormattingEnabled = true;
            this.cbMLShippingType.Location = new System.Drawing.Point(128, 135);
            this.cbMLShippingType.Name = "cbMLShippingType";
            this.cbMLShippingType.Size = new System.Drawing.Size(116, 25);
            this.cbMLShippingType.TabIndex = 39;
            // 
            // lbMLWarranty
            // 
            this.lbMLWarranty.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLWarranty.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLWarranty.Location = new System.Drawing.Point(3, 115);
            this.lbMLWarranty.Name = "lbMLWarranty";
            this.lbMLWarranty.Size = new System.Drawing.Size(119, 17);
            this.lbMLWarranty.TabIndex = 38;
            this.lbMLWarranty.Text = "Garantía";
            this.lbMLWarranty.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cbMLWarranty
            // 
            this.cbMLWarranty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMLWarranty.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbMLWarranty.FormattingEnabled = true;
            this.cbMLWarranty.Location = new System.Drawing.Point(6, 135);
            this.cbMLWarranty.Name = "cbMLWarranty";
            this.cbMLWarranty.Size = new System.Drawing.Size(116, 25);
            this.cbMLWarranty.TabIndex = 37;
            // 
            // lbMLCondition
            // 
            this.lbMLCondition.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLCondition.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLCondition.Location = new System.Drawing.Point(254, 63);
            this.lbMLCondition.Name = "lbMLCondition";
            this.lbMLCondition.Size = new System.Drawing.Size(129, 17);
            this.lbMLCondition.TabIndex = 36;
            this.lbMLCondition.Text = "Condición";
            this.lbMLCondition.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cbMLCondition
            // 
            this.cbMLCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMLCondition.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbMLCondition.FormattingEnabled = true;
            this.cbMLCondition.Location = new System.Drawing.Point(257, 83);
            this.cbMLCondition.Name = "cbMLCondition";
            this.cbMLCondition.Size = new System.Drawing.Size(112, 25);
            this.cbMLCondition.TabIndex = 35;
            // 
            // lbMLDiscount
            // 
            this.lbMLDiscount.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLDiscount.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLDiscount.Location = new System.Drawing.Point(152, 63);
            this.lbMLDiscount.Name = "lbMLDiscount";
            this.lbMLDiscount.Size = new System.Drawing.Size(75, 17);
            this.lbMLDiscount.TabIndex = 34;
            this.lbMLDiscount.Text = "Descuento";
            this.lbMLDiscount.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbMLDiscount
            // 
            this.tbMLDiscount.Location = new System.Drawing.Point(155, 83);
            this.tbMLDiscount.Name = "tbMLDiscount";
            this.tbMLDiscount.Size = new System.Drawing.Size(72, 25);
            this.tbMLDiscount.TabIndex = 33;
            // 
            // lbMLPrice
            // 
            this.lbMLPrice.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMLPrice.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbMLPrice.Location = new System.Drawing.Point(3, 63);
            this.lbMLPrice.Name = "lbMLPrice";
            this.lbMLPrice.Size = new System.Drawing.Size(143, 17);
            this.lbMLPrice.TabIndex = 32;
            this.lbMLPrice.Text = "Precio online";
            this.lbMLPrice.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbMLPrice
            // 
            this.tbMLPrice.Location = new System.Drawing.Point(6, 83);
            this.tbMLPrice.Name = "tbMLPrice";
            this.tbMLPrice.Size = new System.Drawing.Size(143, 25);
            this.tbMLPrice.TabIndex = 31;
            // 
            // lbTitle
            // 
            this.lbTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbTitle.Location = new System.Drawing.Point(3, 6);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(366, 17);
            this.lbTitle.TabIndex = 30;
            this.lbTitle.Text = "Titulo";
            this.lbTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbMLTitle
            // 
            this.tbMLTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMLTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMLTitle.Location = new System.Drawing.Point(6, 29);
            this.tbMLTitle.Name = "tbMLTitle";
            this.tbMLTitle.Size = new System.Drawing.Size(363, 25);
            this.tbMLTitle.TabIndex = 29;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(375, 309);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Imágenes y atributos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lbSKU
            // 
            this.lbSKU.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSKU.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbSKU.Location = new System.Drawing.Point(10, 9);
            this.lbSKU.Name = "lbSKU";
            this.lbSKU.Size = new System.Drawing.Size(215, 17);
            this.lbSKU.TabIndex = 7;
            this.lbSKU.Text = "SKU";
            this.lbSKU.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbSKU.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            // 
            // tbSKU
            // 
            this.tbSKU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSKU.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSKU.Location = new System.Drawing.Point(12, 29);
            this.tbSKU.Name = "tbSKU";
            this.tbSKU.Size = new System.Drawing.Size(215, 25);
            this.tbSKU.TabIndex = 6;
            // 
            // lbNombre
            // 
            this.lbNombre.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombre.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbNombre.Location = new System.Drawing.Point(10, 57);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(215, 17);
            this.lbNombre.TabIndex = 9;
            this.lbNombre.Text = "Nombre";
            this.lbNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbNombre.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            // 
            // tbName
            // 
            this.tbName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbName.Location = new System.Drawing.Point(13, 77);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(392, 25);
            this.tbName.TabIndex = 8;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(233)))), ((int)(((byte)(250)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(131)))), ((int)(((byte)(250)))));
            this.btnCancel.Location = new System.Drawing.Point(216, 525);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(190, 25);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "CANCELAR";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(200)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(13, 525);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(190, 25);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "GRABAR";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label1.Location = new System.Drawing.Point(234, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Categoría";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            // 
            // cbCategory
            // 
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(237, 29);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(169, 25);
            this.cbCategory.TabIndex = 16;
            // 
            // ProductoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(422, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCategory);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbNombre);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbSKU);
            this.Controls.Add(this.tbSKU);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProductoForm";
            this.Load += new System.EventHandler(this.ProductoForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            this.tabControl1.ResumeLayout(false);
            this.tpProduct.ResumeLayout(false);
            this.tpProduct.PerformLayout();
            this.tpPublication.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpProduct;
        private System.Windows.Forms.TabPage tpPublication;
        private System.Windows.Forms.Label lbSKU;
        private System.Windows.Forms.TextBox tbSKU;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbBasePrice;
        private System.Windows.Forms.Label lbDescription;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Button btnAddAlarm;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lbMLDescription;
        private System.Windows.Forms.TextBox tbMLDescription;
        private System.Windows.Forms.Label lbMLShippingType;
        private System.Windows.Forms.ComboBox cbMLShippingType;
        private System.Windows.Forms.Label lbMLWarranty;
        private System.Windows.Forms.ComboBox cbMLWarranty;
        private System.Windows.Forms.Label lbMLCondition;
        private System.Windows.Forms.ComboBox cbMLCondition;
        private System.Windows.Forms.Label lbMLDiscount;
        private System.Windows.Forms.MaskedTextBox tbMLDiscount;
        private System.Windows.Forms.Label lbMLPrice;
        private System.Windows.Forms.MaskedTextBox tbMLPrice;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.TextBox tbMLTitle;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnPublish;
        private System.Windows.Forms.Label lbMLStatus;
        private MoneyTextBox tbPrice;
        private MoneyTextBox tbBasePrice;
    }
}