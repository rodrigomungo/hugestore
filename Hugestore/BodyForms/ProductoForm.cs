﻿using Hugestore.Controllers;
using Hugestore.Controls;
using Hugestore.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Hugestore.BodyForms
{
    public partial class ProductoForm : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(IntPtr hwnd, int wmsg, int wparam, int lparam);

        public ProductoForm()
        {
            InitializeComponent();
        }

        private void ProductoForm_Load(object sender, EventArgs e)
        {
            _cargarCategorias();
        }

        private void Control_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void _cargarCategorias()
        {
            cbCategory.Items.Clear();
            List<CategoriaView> categorias = CategoriaController.Current.GetAll();
            ComboBoxItem[] items = categorias.Select(c => new ComboBoxItem()
            {
                Text = c.Nombre,
                Value = c.Id
            }).ToArray();
            cbCategory.Items.AddRange(items);
        }

        private void btnAddAlarm_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ComboBoxItem item = (ComboBoxItem)cbCategory.SelectedItem;
                int categoriaId = 0;
                if (item != null)
                {
                    categoriaId = (int)item.Value;
                }

                ProductoView view = new ProductoView(EntityState.Added)
                {
                    SKU = tbSKU.Text,
                    Nombre = tbName.Text,
                    Descripcion = tbDescription.Text,
                    Precio = tbPrice.Value,
                    PrecioBase = tbBasePrice.Value,
                    CategoriaId = categoriaId,
                };
                ProductoController.Current.SaveChanges(view);
                MessageBox.Show("El producto fue guardado", "Hugestore", MessageBoxButtons.OK);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {

        }
    }
}
