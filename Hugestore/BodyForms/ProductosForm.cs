﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using Hugestore.Controllers;
using Hugestore.Controls;
using Hugestore.ViewModels;

namespace Hugestore.BodyForms
{
    public partial class ProductosForm : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(IntPtr hwnd, int wmsg, int wparam, int lparam);

        private const int LIMIT = 20;
        private int _offset = 0;

        public ProductosForm()
        {
            InitializeComponent();
        }

        private void ProductosForm_Load(object sender, EventArgs e)
        {
            _cargarCategorias();
            _cargarProductos(new Producto(), _offset);
        }

        private void Control_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void _cargarCategorias()
        {
            cbCategory.Items.Clear();
            List<CategoriaView> categorias = CategoriaController.Current.GetAll();
            ComboBoxItem[] items = categorias.Select(c => new ComboBoxItem()
            {
                Text = c.Nombre,
                Value = c.Id
            }).ToArray();
            cbCategory.Items.AddRange(items);
        }

        private void _cargarProductos(Producto producto, int start)
        {
            List<Producto> productos = ProductoService.Current.Listar(producto, LIMIT, start);
            dgvProducts.Rows.Clear();
            foreach (Producto p in productos)
            {
                CategoriaView categoria = CategoriaController.Current.GetAll(c => c.Id == p.CategoriaId)[0];
                DataGridViewRow fila = new DataGridViewRow();
                fila.CreateCells(dgvProducts);
                fila.Cells[0].Value = p.Id;
                fila.Cells[1].Value = p.SKU;
                fila.Cells[2].Value = p.Nombre;
                fila.Cells[3].Value = p.Precio;
                fila.Cells[4].Value = categoria.Nombre;
                dgvProducts.Rows.Add(fila);
            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Producto producto = new Producto();
            producto.SKU = tbSkuName.Text;
            producto.Nombre = tbSkuName.Text;
            ComboBoxItem item = (ComboBoxItem)cbCategory.SelectedItem;
            producto.CategoriaId = (int)item.Value;
            _offset = 0;
            _cargarProductos(producto, _offset);
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            CategoriaForm form = new CategoriaForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                _cargarCategorias();
            }
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            ProductoForm form = new ProductoForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                _offset = 0;
                _cargarProductos(new Producto(), _offset);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {

        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            
        }
    }
}
