﻿using System;
using Domain.Hugestore;
using SL.Hugestore.Services.Extensions;

namespace Hugestore.Context
{
    public static class EmpresaContext
    {
        static Empresa _credentials;

        public static Empresa Credentials
        {
            get
            {
                return _credentials;
            }
        }

        public static bool IsAuthorized()
        {
            if (_credentials is null)
            {
                return false;
            }

            bool expired = DateTimeOffset.UtcNow > _credentials.FechaAutenticacion.AddSeconds(_credentials.Expiracion);
            if (!_credentials.Token.IsNullOrEmpty() && !expired)
            {
                return true;
            }

            return false;
        }

        public static void Authorize(Empresa empresa)
        {
            _credentials = empresa;
        }
    }
}
