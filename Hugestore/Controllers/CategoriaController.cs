﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using Hugestore.Validators;
using Hugestore.ViewModels;
using Hugestore.ViewModels.Helpers;

namespace Hugestore.Controllers
{
    public sealed class CategoriaController : IController<CategoriaView, Categoria, int>
    {
        #region singleton
        private readonly static CategoriaController _instance = new CategoriaController();

        public static CategoriaController Current
        {
            get
            {
                return _instance;
            }
        }

        private CategoriaController() { }
        #endregion

        public List<CategoriaView> GetAll(Func<Categoria, bool> filter)
        {
            IEnumerable<Categoria> categorias = CategoriaService.Current.Listar();
            var categoriasFiltradas = (filter != null) ? categorias.Where(filter).ToList() : categorias.ToList();
            var categoriasView = MapperHelper.GetMapper().Map<List<Categoria>, List<CategoriaView>>(categoriasFiltradas);
            return categoriasView;
        }

        public List<CategoriaView> GetAll()
        {
            IEnumerable<Categoria> categorias = CategoriaService.Current.Listar();
            var categoriasView = MapperHelper.GetMapper().Map<List<Categoria>, List<CategoriaView>>(categorias.ToList());
            return categoriasView;
        }

        public CategoriaView GetOne(int id)
        {
            var categoria = CategoriaService.Current.ObtenerUno(id);
            var categoriaView = MapperHelper.GetMapper().Map<Categoria, CategoriaView>(categoria);

            return categoriaView;
        }

        [ViewValidator]
        public void SaveChanges(CategoriaView view)
        {
            try
            {
                Categoria categoria = MapperHelper.GetMapper().Map<CategoriaView, Categoria>(view);
                switch (view.State)
                {
                    case EntityState.Added:
                        CategoriaService.Current.Agregar(categoria);
                        break;

                    case EntityState.Modified:
                        CategoriaService.Current.Actualizar(categoria);
                        break;
                }

                view.State = EntityState.Unchanged;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
