﻿using System;
using System.Collections.Generic;
using Hugestore.Validators;

namespace Hugestore.Controllers
{
    public interface IController<T, U, V>
    {
        List<T> GetAll(Func<U, bool> filter);

        T GetOne(V id);

        [ViewValidator]
        void SaveChanges(T view);
    }
}
