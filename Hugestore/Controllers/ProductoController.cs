﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using Hugestore.Validators;
using Hugestore.ViewModels;
using Hugestore.ViewModels.Helpers;

namespace Hugestore.Controllers
{
    public sealed class ProductoController : IController<ProductoView, Producto, Guid>
    {
        #region singleton
        private readonly static ProductoController _instance = new ProductoController();

        public static ProductoController Current
        {
            get
            {
                return _instance;
            }
        }

        private ProductoController() { }
        #endregion

        public List<ProductoView> GetAll(Func<Producto, bool> filter)
        {
            IEnumerable<Producto> productos = ProductoService.Current.Listar(new Producto(), 10, 0);
            var productosFiltrados = (filter != null) ? productos.Where(filter).ToList() : productos.ToList();
            var productosView = MapperHelper.GetMapper().Map<List<Producto>, List<ProductoView>>(productosFiltrados);
            return productosView;
        }

        public List<ProductoView> GetAll(Producto producto, int limit, int offset)
        {
            IEnumerable<Producto> productos = ProductoService.Current.Listar(producto, limit, offset);
            var productosView = MapperHelper.GetMapper().Map<List<Producto>, List<ProductoView>>(productos.ToList());
            return productosView;
        }

        public ProductoView GetOne(Guid id)
        {
            var producto = ProductoService.Current.ObtenerUno(id);
            var productoView = MapperHelper.GetMapper().Map<Producto, ProductoView>(producto);
            return productoView;
        }

        [ViewValidator]
        public void SaveChanges(ProductoView view)
        {
            try
            {
                Producto producto = MapperHelper.GetMapper().Map<ProductoView, Producto>(view);
                switch (view.State)
                {
                    case EntityState.Added:
                        ProductoService.Current.Agregar(producto);
                        break;

                    case EntityState.Modified:
                        ProductoService.Current.Actualizar(producto);
                        break;
                }

                view.State = EntityState.Unchanged;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
