﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using SL.Hugestore.Services.Security;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace Hugestore
{
    public partial class LogInForm : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(IntPtr hwnd, int wmsg, int wparam, int lparam);

        public LogInForm()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string userName = tbUserName.Text;
            string password = tbPassword.Text;
            Usuario usuario = UserFacade.Current.LogIn(new Usuario() { NombreUsuario = userName, Clave = password });
            if (usuario != null)
            {
                this.Hide();
                tbPassword.Text = "";
                SessionManager.Login(usuario);
                Main mainForm = new Main();
                if (mainForm.ShowDialog() == DialogResult.OK)
                {
                    this.Show();
                }
            }
            else
            {
                MessageBox.Show("Usuario o contraseña incorrectos");
            }
        }

        private void Control_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
