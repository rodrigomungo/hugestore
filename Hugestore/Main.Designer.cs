﻿
namespace Hugestore
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.pHeader = new System.Windows.Forms.Panel();
            this.lbLanguage = new System.Windows.Forms.Label();
            this.cbLanguage = new System.Windows.Forms.ComboBox();
            this.lbFormTitle = new System.Windows.Forms.Label();
            this.pBody = new System.Windows.Forms.Panel();
            this.pFooter = new System.Windows.Forms.Panel();
            this.lbAuth = new System.Windows.Forms.Label();
            this.btnAuth = new System.Windows.Forms.Button();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pHeader.SuspendLayout();
            this.pFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.AutoSize = false;
            this.menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.menu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menu.Size = new System.Drawing.Size(187, 602);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            this.menu.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            // 
            // pHeader
            // 
            this.pHeader.Controls.Add(this.lbLanguage);
            this.pHeader.Controls.Add(this.cbLanguage);
            this.pHeader.Controls.Add(this.lbFormTitle);
            this.pHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pHeader.Location = new System.Drawing.Point(187, 0);
            this.pHeader.Name = "pHeader";
            this.pHeader.Size = new System.Drawing.Size(770, 63);
            this.pHeader.TabIndex = 1;
            this.pHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            // 
            // lbLanguage
            // 
            this.lbLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLanguage.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbLanguage.Location = new System.Drawing.Point(676, 9);
            this.lbLanguage.Name = "lbLanguage";
            this.lbLanguage.Size = new System.Drawing.Size(82, 17);
            this.lbLanguage.TabIndex = 19;
            this.lbLanguage.Text = "Idioma";
            this.lbLanguage.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cbLanguage
            // 
            this.cbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbLanguage.FormattingEnabled = true;
            this.cbLanguage.Location = new System.Drawing.Point(679, 29);
            this.cbLanguage.Name = "cbLanguage";
            this.cbLanguage.Size = new System.Drawing.Size(82, 25);
            this.cbLanguage.TabIndex = 18;
            this.cbLanguage.SelectedIndexChanged += new System.EventHandler(this.cbLanguage_SelectedIndexChanged);
            // 
            // lbFormTitle
            // 
            this.lbFormTitle.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFormTitle.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbFormTitle.Location = new System.Drawing.Point(121, 13);
            this.lbFormTitle.Name = "lbFormTitle";
            this.lbFormTitle.Size = new System.Drawing.Size(513, 37);
            this.lbFormTitle.TabIndex = 6;
            this.lbFormTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pBody
            // 
            this.pBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pBody.Location = new System.Drawing.Point(187, 63);
            this.pBody.Name = "pBody";
            this.pBody.Size = new System.Drawing.Size(770, 539);
            this.pBody.TabIndex = 2;
            this.pBody.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            // 
            // pFooter
            // 
            this.pFooter.Controls.Add(this.btnAuth);
            this.pFooter.Controls.Add(this.lbAuth);
            this.pFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pFooter.Location = new System.Drawing.Point(187, 559);
            this.pFooter.Name = "pFooter";
            this.pFooter.Size = new System.Drawing.Size(770, 43);
            this.pFooter.TabIndex = 3;
            // 
            // lbAuth
            // 
            this.lbAuth.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuth.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbAuth.Location = new System.Drawing.Point(10, 10);
            this.lbAuth.Name = "lbAuth";
            this.lbAuth.Size = new System.Drawing.Size(521, 22);
            this.lbAuth.TabIndex = 20;
            this.lbAuth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAuth
            // 
            this.btnAuth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(200)))));
            this.btnAuth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAuth.FlatAppearance.BorderSize = 0;
            this.btnAuth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAuth.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuth.ForeColor = System.Drawing.Color.White;
            this.btnAuth.Location = new System.Drawing.Point(537, 3);
            this.btnAuth.Name = "btnAuth";
            this.btnAuth.Size = new System.Drawing.Size(224, 37);
            this.btnAuth.TabIndex = 21;
            this.btnAuth.Text = "AUTENTICAR EN MERCADOLIBRE";
            this.btnAuth.UseVisualStyleBackColor = false;
            this.btnAuth.Click += new System.EventHandler(this.btnAuth_Click);
            // 
            // bgWorker
            // 
            this.bgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
            this.bgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_RunWorkerCompleted);
            // 
            // timer
            // 
            this.timer.Interval = 5000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(957, 602);
            this.Controls.Add(this.pFooter);
            this.Controls.Add(this.pBody);
            this.Controls.Add(this.pHeader);
            this.Controls.Add(this.menu);
            this.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menu;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hugestore";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Control_MouseDown);
            this.pHeader.ResumeLayout(false);
            this.pFooter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.Panel pHeader;
        private System.Windows.Forms.Panel pBody;
        private System.Windows.Forms.Label lbFormTitle;
        private System.Windows.Forms.Label lbLanguage;
        private System.Windows.Forms.ComboBox cbLanguage;
        private System.Windows.Forms.Panel pFooter;
        private System.Windows.Forms.Label lbAuth;
        private System.Windows.Forms.Button btnAuth;
        private System.ComponentModel.BackgroundWorker bgWorker;
        private System.Windows.Forms.Timer timer;
    }
}