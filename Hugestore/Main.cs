﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Hugestore.BodyForms;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;
using SL.Hugestore.Services.I18n;
using SL.Hugestore.Services.Extensions;
using Hugestore.Controls;
using System.Drawing;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using Hugestore.Context;

namespace Hugestore
{
    public partial class Main : Form, ILanguageObserver
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(IntPtr hwnd, int wmsg, int wparam, int lparam);

        private string WebAppBaseUrl = ConfigurationManager.AppSettings["WebAppBaseUrl"];

        private Usuario _usuario;
        private string[] LANGUAGES = new string[] { "es-ES", "en-US" };

        public Main()
        {
            InitializeComponent();
            _usuario = (Usuario)SessionManager.Session;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SessionManager.Suscribir(this);
            _loadEnterprice();
            _loadMenuItems();
            _translate();
            _loadLanguages();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            SessionManager.Desuscribir(this);
        }

        public void UpdateLanguage(CultureInfo cultura)
        {
            _translate();
        }

        private void Control_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void _openFormChild(object formChild)
        {
            if (pBody.Controls.Count > 0)
            {
                pBody.Controls.RemoveAt(0);
            }

            Form formInside = formChild as Form;
            formInside.TopLevel = false;
            formInside.Dock = DockStyle.Fill;
            pBody.Controls.Add(formInside);
            pBody.Tag = formInside;
            formInside.Show();
        }

        private void _loadEnterprice()
        {
            Console.WriteLine("log");
            try
            {
                long aplicacionId;
                long.TryParse(ConfigurationManager.AppSettings["MercadoLibreApplicationId"], out aplicacionId);
                Empresa empresa = EmpresaService.Current.ObtenerUno(aplicacionId);
                EmpresaContext.Authorize(empresa);
                if (EmpresaContext.IsAuthorized())
                {
                    lbAuth.Text = "Autorizado por MercadoLibre";
                    lbAuth.ForeColor = Color.DarkGreen;
                    btnAuth.Visible = false;
                }
                else
                {
                    lbAuth.Text = "No autorizado por MercadoLibre";
                    lbAuth.ForeColor = Color.DarkRed;
                    btnAuth.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lbAuth.Text = ex.Message;
                lbAuth.ForeColor = Color.DarkOrange;
                btnAuth.Visible = true;
            }
        }

        private void _loadFormTitle(string text)
        {
            lbFormTitle.Tag = text;
            lbFormTitle.Text = lbFormTitle.Tag.ToString().Traducir();
        }

        private void _translate()
        {
            foreach(ToolStripMenuItem item in menu.Items)
            {
                if (item.Name != "userNameToolStripMenuItem")
                {
                    item.Text = item.Tag.ToString().Traducir();
                }
            }

            if (lbFormTitle.Tag != null)
            {
                lbFormTitle.Text = lbFormTitle.Tag.ToString().Traducir();
            }
            
        }

        private void _loadLanguages()
        {
            cbLanguage.Items.Clear();
            ComboBoxItem[] items = LANGUAGES.Select(l => new ComboBoxItem()
            {
                Text = l,
                Value = new CultureInfo(l)
            }).ToArray();
            int selectedIndex = 0;
            for(int i = 0; i < items.Length; i++)
            {
                CultureInfo culture = (CultureInfo)items[i].Value;
                if (culture.Name.Equals(Thread.CurrentThread.CurrentUICulture.Name))
                {
                    selectedIndex = i;
                }
            }

            cbLanguage.Items.AddRange(items);
            cbLanguage.SelectedIndex = selectedIndex;
        }

        private void _loadMenuItems()
        {
            int marginTop = 530;
            Size size = new Size(160, 30);
            if (_usuario.TieneRol("Productos"))
            {
                addMenuItem("productosToolStripMenuItem", "Products", "Productos", new EventHandler(productosToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Pedidos Pendientes"))
            {
                addMenuItem("pedidosPendientesToolStripMenuItem", "Pending Orders", "Pedidos Pendientes", new EventHandler(pedidosPendientesToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Ingresar Stock"))
            {
                addMenuItem("ingresarStockToolStripMenuItem", "Enter Stock", "Ingresar Stock", new EventHandler(ingresarStockToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Listado de Stock"))
            {
                addMenuItem("listadoDeStockToolStripMenuItem", "Stock List", "Listado de Stock", new EventHandler(listadoDeStockToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Alarmas de Stock"))
            {
                addMenuItem("alarmasDeStockToolStripMenuItem", "Stock Alarms", "Alarmas de Stock", new EventHandler(alarmasDeStockToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Depósitos"))
            {
                addMenuItem("depósitosToolStripMenuItem", "Deposits", "Depósitos", new EventHandler(depósitosToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Facturación"))
            {
                addMenuItem("facturaciónToolStripMenuItem", "Invoices", "Facturación", new EventHandler(facturaciónToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Estadísticas"))
            {
                addMenuItem("estadísticasToolStripMenuItem", "Statistics", "Estadísticas", new EventHandler(estadísticasToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            if (_usuario.TieneRol("Auditoría"))
            {
                addMenuItem("auditoríaToolStripMenuItem", "Audit", "Auditoría", new EventHandler(auditoríaToolStripMenuItem_Click), size);
                marginTop = marginTop - 30;
            }

            ToolStripMenuItem userNameToolStripMenuItem = new ToolStripMenuItem();
            userNameToolStripMenuItem.AutoSize = false;
            userNameToolStripMenuItem.Enabled = false;
            userNameToolStripMenuItem.Margin = new Padding(0, marginTop, 0, 0);
            userNameToolStripMenuItem.Name = "userNameToolStripMenuItem";
            userNameToolStripMenuItem.Size = new Size(160, 22);
            userNameToolStripMenuItem.Text = _usuario.NombreCompleto;
            menu.Items.Add(userNameToolStripMenuItem);

            addMenuItem("cerrarSesiónToolStripMenuItem", "Logout", "Cerrar Sesión", new EventHandler(cerrarSesiónToolStripMenuItem_Click), size);
        }

        private void addMenuItem(string name, string tag, string text, EventHandler eventHandler, Size size)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem();
            menuItem.AutoSize = false;
            menuItem.Name = name;
            menuItem.Size = size;
            menuItem.Tag = tag;
            menuItem.Text = text;
            menuItem.Click += eventHandler;
            menu.Items.Add(menuItem);
        }

        private void cbLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)cbLanguage.Items[cbLanguage.SelectedIndex];
            CultureInfo culture = (CultureInfo)item.Value;
            SessionManager.CambiarIdioma(culture);
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductosForm form = new ProductosForm();
            _openFormChild(form);
            _loadFormTitle("Products");
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro/a de cerrar la sesión?", "Hugestore", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _usuario = null;
                SessionManager.Logout();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void pedidosPendientesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ingresarStockToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listadoDeStockToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void alarmasDeStockToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void depósitosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void facturaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void estadísticasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void auditoríaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnAuth_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(WebAppBaseUrl);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            bgWorker.RunWorkerAsync();
        }

        private void bgWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            _loadEnterprice();
        }

        private void bgWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {

        }
    }
}
