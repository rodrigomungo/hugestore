﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace Hugestore.ViewModels
{
    public class CategoriaView
    {
        public CategoriaView(EntityState state = EntityState.Unchanged)
        {
            State = state;
        }

        public CategoriaView(string nombre, string descripcion) : this()
        {
            Nombre = nombre;
            Descripcion = descripcion;
        }

        public CategoriaView(int categoriaId, string nombre, string descripcion) : this(nombre, descripcion)
        {
            Id = categoriaId;
        }

        [Browsable(false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        [Browsable(false)]
        public EntityState State { get; set; }

        public override string ToString()
        {
            return "Categoria";
        }
    }
}
