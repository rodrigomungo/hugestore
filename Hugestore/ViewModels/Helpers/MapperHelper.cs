﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Hugestore;

namespace Hugestore.ViewModels.Helpers
{
    public static class MapperHelper
    {
        readonly private static IMapper iMapper;
        static MapperHelper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CategoriaView, Categoria>();
                cfg.CreateMap<Categoria, CategoriaView>();
                cfg.CreateMap<ProductoView, Producto>();
                cfg.CreateMap<Producto, ProductoView>();

                //Setear configurador de mappers...


                //CustomerView.FirstName -> Customer.Name


                cfg.CreateMap<List<object>, List<object>>().ConvertUsing<Resolver>();
            });

            iMapper = config.CreateMapper();
        }
        public static IMapper GetMapper()
        {
            return iMapper;
        }

        private class Resolver : ITypeConverter<List<object>, List<object>>
        {
            public List<object> Convert(List<object> source, List<object> destination, ResolutionContext context)
            {
                var objects = new List<object>();

                foreach (var obj in source)
                {
                    var destinationType = context.ConfigurationProvider.GetAllTypeMaps().First(x => x.SourceType == obj.GetType()).DestinationType;
                    var target = context.Mapper.Map(obj, obj.GetType(), destinationType);
                    objects.Add(target);
                }

                return objects;
            }
        }
    }
}
