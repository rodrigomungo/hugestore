﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hugestore.ViewModels
{
    public class ProductoView
    {
        public ProductoView(EntityState state = EntityState.Unchanged)
        {
            State = state;
        }

        public ProductoView(string sku, string nombre, string descripcion, decimal costo, decimal precio) 
            : this()
        {
            SKU = sku;
            Nombre = nombre;
            Descripcion = descripcion;
            PrecioBase = costo;
            Precio = precio;
        }

        public ProductoView(Guid productoId, string sku, string nombre, string descripcion, decimal costo, decimal precio) 
            : this(sku, nombre, descripcion, costo, precio)
        {
            Id = productoId;
        }

        [Browsable(false)]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El SKU es requerido")]
        public string SKU { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public decimal PrecioBase { get; set; }

        public decimal Precio { get; set; }

        [RegularExpression("[1-9]+", ErrorMessage = "La categoria es requerida")]
        public int CategoriaId { get; set; }

        [Browsable(false)]
        public EntityState State { get; set; }

        public override string ToString()
        {
            return "Categoria";
        }
    }
}
