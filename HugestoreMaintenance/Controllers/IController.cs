﻿using System;
using System.Collections.Generic;

namespace HugestoreMaintenance.Controllers
{
    public interface IController<T>
    {
        List<T> GetAll();
        void Restore(string database, string path);
        void Backup(string database, string bkpPath);
    }
}
