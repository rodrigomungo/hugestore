﻿using System;
using System.Collections.Generic;
using System.Linq;
using SL.Hugestore.DAL.Factories;
using SL.Hugestore.Services.Extensions;

namespace HugestoreMaintenance.Controllers
{
    public sealed class MaintenanceController : IController<string>
    {
        #region singleton
        private readonly static MaintenanceController _instance = new MaintenanceController();

        public static MaintenanceController Current
        {
            get
            {
                return _instance;
            }
        }

        private MaintenanceController() { }
        #endregion

        public void Backup(string database, string bkpPath)
        {
            if (database.IsNullOrEmpty())
            {
                throw new Exception("El campo base de datos es requerido");
            }

            if (bkpPath.IsNullOrEmpty())
            {
                throw new Exception("El campo archivo es requerido");
            }

            MaintenanceFactory.Current.MaintenanceRepository.Backup(database, bkpPath);
        }

        public List<string> GetAll()
        {
            return MaintenanceFactory.Current.MaintenanceRepository.GetDatabases().ToList();
        }

        public void Restore(string database, string path)
        {
            if (path.IsNullOrEmpty())
            {
                throw new Exception("El campo archivo es requerido");
            }

            if (database.IsNullOrEmpty())
            {
                throw new Exception("El campo base de datos es requerido");
            }

            MaintenanceFactory.Current.MaintenanceRepository.Backup(database, path);
        }
    }
}
