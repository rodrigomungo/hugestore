﻿
namespace HugestoreMaintenance
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabBackup = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tbRestoreFile = new System.Windows.Forms.TextBox();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.lbUserName = new System.Windows.Forms.Label();
            this.btnRestore = new System.Windows.Forms.Button();
            this.cbDatabases = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDatabasesBkp = new System.Windows.Forms.ComboBox();
            this.btnBackup = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOpenFileBkp = new System.Windows.Forms.Button();
            this.tbBackupFile = new System.Windows.Forms.TextBox();
            this.openFileDialogBkp = new System.Windows.Forms.OpenFileDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.tabBackup.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // tabBackup
            // 
            this.tabBackup.Controls.Add(this.tabPage1);
            this.tabBackup.Controls.Add(this.tabPage2);
            this.tabBackup.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.tabBackup.Location = new System.Drawing.Point(12, 32);
            this.tabBackup.Name = "tabBackup";
            this.tabBackup.SelectedIndex = 0;
            this.tabBackup.Size = new System.Drawing.Size(280, 225);
            this.tabBackup.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.cbDatabasesBkp);
            this.tabPage1.Controls.Add(this.btnBackup);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.btnOpenFileBkp);
            this.tabPage1.Controls.Add(this.tbBackupFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(272, 195);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Backup";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.cbDatabases);
            this.tabPage2.Controls.Add(this.btnRestore);
            this.tabPage2.Controls.Add(this.lbUserName);
            this.tabPage2.Controls.Add(this.openFileBtn);
            this.tabPage2.Controls.Add(this.tbRestoreFile);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(272, 195);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Restore";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tbRestoreFile
            // 
            this.tbRestoreFile.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRestoreFile.Location = new System.Drawing.Point(6, 33);
            this.tbRestoreFile.Name = "tbRestoreFile";
            this.tbRestoreFile.Size = new System.Drawing.Size(218, 22);
            this.tbRestoreFile.TabIndex = 0;
            // 
            // openFileBtn
            // 
            this.openFileBtn.Location = new System.Drawing.Point(230, 33);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(32, 23);
            this.openFileBtn.TabIndex = 1;
            this.openFileBtn.Text = "...";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.openFileBtn_Click);
            // 
            // lbUserName
            // 
            this.lbUserName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUserName.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lbUserName.Location = new System.Drawing.Point(6, 14);
            this.lbUserName.Name = "lbUserName";
            this.lbUserName.Size = new System.Drawing.Size(256, 16);
            this.lbUserName.TabIndex = 3;
            this.lbUserName.Text = "Archivo";
            this.lbUserName.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // btnRestore
            // 
            this.btnRestore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(200)))));
            this.btnRestore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRestore.FlatAppearance.BorderSize = 0;
            this.btnRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestore.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestore.ForeColor = System.Drawing.Color.White;
            this.btnRestore.Location = new System.Drawing.Point(6, 143);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(256, 46);
            this.btnRestore.TabIndex = 5;
            this.btnRestore.Text = "RESTAURAR";
            this.btnRestore.UseVisualStyleBackColor = false;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // cbDatabases
            // 
            this.cbDatabases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDatabases.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbDatabases.FormattingEnabled = true;
            this.cbDatabases.Location = new System.Drawing.Point(9, 89);
            this.cbDatabases.Name = "cbDatabases";
            this.cbDatabases.Size = new System.Drawing.Size(253, 25);
            this.cbDatabases.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label1.Location = new System.Drawing.Point(6, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Base de datos";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label2.Location = new System.Drawing.Point(8, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(256, 16);
            this.label2.TabIndex = 24;
            this.label2.Text = "Base de datos";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // cbDatabasesBkp
            // 
            this.cbDatabasesBkp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDatabasesBkp.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbDatabasesBkp.FormattingEnabled = true;
            this.cbDatabasesBkp.Location = new System.Drawing.Point(11, 31);
            this.cbDatabasesBkp.Name = "cbDatabasesBkp";
            this.cbDatabasesBkp.Size = new System.Drawing.Size(253, 25);
            this.cbDatabasesBkp.TabIndex = 23;
            this.cbDatabasesBkp.SelectedIndexChanged += new System.EventHandler(this.cbDatabasesBkp_SelectedIndexChanged);
            // 
            // btnBackup
            // 
            this.btnBackup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(200)))));
            this.btnBackup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBackup.FlatAppearance.BorderSize = 0;
            this.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackup.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackup.ForeColor = System.Drawing.Color.White;
            this.btnBackup.Location = new System.Drawing.Point(8, 139);
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(256, 46);
            this.btnBackup.TabIndex = 22;
            this.btnBackup.Text = "BACKUP";
            this.btnBackup.UseVisualStyleBackColor = false;
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label3.Location = new System.Drawing.Point(8, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(256, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "Archivo";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // btnOpenFileBkp
            // 
            this.btnOpenFileBkp.Enabled = false;
            this.btnOpenFileBkp.Location = new System.Drawing.Point(232, 89);
            this.btnOpenFileBkp.Name = "btnOpenFileBkp";
            this.btnOpenFileBkp.Size = new System.Drawing.Size(32, 23);
            this.btnOpenFileBkp.TabIndex = 20;
            this.btnOpenFileBkp.Text = "...";
            this.btnOpenFileBkp.UseVisualStyleBackColor = true;
            this.btnOpenFileBkp.Click += new System.EventHandler(this.btnOpenFileBkp_Click);
            // 
            // tbBackupFile
            // 
            this.tbBackupFile.Enabled = false;
            this.tbBackupFile.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBackupFile.Location = new System.Drawing.Point(11, 90);
            this.tbBackupFile.Name = "tbBackupFile";
            this.tbBackupFile.Size = new System.Drawing.Size(218, 22);
            this.tbBackupFile.TabIndex = 19;
            // 
            // openFileDialogBkp
            // 
            this.openFileDialogBkp.CheckFileExists = false;
            this.openFileDialogBkp.Filter = "Files|*.bak";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 30000;
            this.toolTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(104)))), ((int)(((byte)(200)))));
            this.toolTip.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.toolTip.InitialDelay = 500;
            this.toolTip.IsBalloon = true;
            this.toolTip.ReshowDelay = 100;
            this.toolTip.ShowAlways = true;
            // 
            // pbHelp
            // 
            this.pbHelp.Image = global::HugestoreMaintenance.Properties.Resources.question;
            this.pbHelp.Location = new System.Drawing.Point(267, 12);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(18, 18);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHelp.TabIndex = 1;
            this.pbHelp.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 269);
            this.Controls.Add(this.pbHelp);
            this.Controls.Add(this.tabBackup);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hugestore Maintenance";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabBackup.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabBackup;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button openFileBtn;
        private System.Windows.Forms.TextBox tbRestoreFile;
        private System.Windows.Forms.Label lbUserName;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDatabases;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbDatabasesBkp;
        private System.Windows.Forms.Button btnBackup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOpenFileBkp;
        private System.Windows.Forms.TextBox tbBackupFile;
        private System.Windows.Forms.OpenFileDialog openFileDialogBkp;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.PictureBox pbHelp;
    }
}

