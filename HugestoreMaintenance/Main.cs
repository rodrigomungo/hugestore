﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Windows.Forms;
using HugestoreMaintenance.Controllers;
using HugestoreMaintenance.Controls;

namespace HugestoreMaintenance
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            _cargarBasesDeDatos(cbDatabases);
            _cargarBasesDeDatos(cbDatabasesBkp);
            toolTip.SetToolTip(pbHelp, "Para realizar restauraciones debes seleccionar un archivo .bak y luego seleccionar la base de datos.\nPara realizar backups debes seleccionar la base de datos primero, ya que esta será utilizada para\nnombrar el archivo .bak al seleccionar la carpeta donde se quiere almacenar.");
        }

        private void _cargarBasesDeDatos(ComboBox combo)
        {
            combo.Items.Clear();
            List<string> databases = MaintenanceController.Current.GetAll();
            ComboBoxItem[] items = databases.Select(db => new ComboBoxItem()
            {
                Text = db,
                Value = db
            }).ToArray();
            combo.Items.AddRange(items);
        }

        private void openFileBtn_Click(object sender, EventArgs e)
        {
            openDialog(openFileDialog, tbRestoreFile);
        }

        private void btnOpenFileBkp_Click(object sender, EventArgs e)
        {
            openDialog(openFileDialogBkp, tbBackupFile);
        }

        private void openDialog(OpenFileDialog dialog, TextBox textBox)
        {
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    textBox.Text = dialog.FileName;
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Error message: {ex.Message}\n\nDetails:\n\n{ex.StackTrace}");
                }
            }
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)cbDatabases.SelectedItem;
            string database = "";
            if (item != null)
            {
                database = item.Value.ToString();
            }

            try
            {
                MaintenanceController.Current.Restore(database, tbRestoreFile.Text);
                MessageBox.Show("La base de datos se restauró correctamente!", "Hugestore Maintenance");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            ComboBoxItem item = (ComboBoxItem)cbDatabasesBkp.SelectedItem;
            string database = "";
            if (item != null)
            {
                database = item.Value.ToString();
            }

            try
            {
                MaintenanceController.Current.Backup(database, tbBackupFile.Text);
                MessageBox.Show("El backup se finalizó correctamente!", "Hugestore Maintenance");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void cbDatabasesBkp_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbBackupFile.Enabled = false;
            tbBackupFile.Text = String.Empty;
            btnOpenFileBkp.Enabled = false;
            ComboBox combo = sender as ComboBox;
            ComboBoxItem item = (ComboBoxItem)combo.SelectedItem;
            string database = "";
            if (item != null)
            {
                database = item.Value.ToString();
                tbBackupFile.Enabled = true;
                btnOpenFileBkp.Enabled = true;
            }

            string date = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            openFileDialogBkp.FileName = $"{database}_{date}.bak";
        }
    }
}
