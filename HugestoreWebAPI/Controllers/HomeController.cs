﻿using System;
using System.Configuration;
using System.Web.Mvc;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using SL.Hugestore.Services.MercadoLibreClient;

namespace HugestoreWebAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            long aplicacionId;
            long.TryParse(ConfigurationManager.AppSettings["MercadoLibreApplicationId"], out aplicacionId);
            string redirectUri = ConfigurationManager.AppSettings["MercadoLibreRedirectUri"];
            Empresa empresa = EmpresaService.Current.ObtenerUno(aplicacionId);
            MercadoLibreManager _meliClient = new MercadoLibreManager(empresa.AplicacionId, empresa.ClaveSecreta, redirectUri);

            ViewBag.Link = _meliClient.GetAuthUrl();

            return View();
        }
    }
}
