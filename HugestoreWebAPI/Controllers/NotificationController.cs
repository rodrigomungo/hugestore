﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using SL.Hugestore.Domain.MercadoLibre.Request;

namespace HugestoreWebAPI.Controllers
{
    public class NotificationController : ApiController
    {
        // POST api/notifications
        public HttpResponseMessage Post([FromBody] NotificationRequest body)
        {
            Notificacion notificacion = new Notificacion()
            {
                Recurso = body.Resource,
                Topico = body.Topic,
            };

            try
            {
                NotificacionService.Current.Agregar(notificacion);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = ex.Message, Status = HttpStatusCode.InternalServerError, Code = "internal_server_error" });
            }
        }
    }
}