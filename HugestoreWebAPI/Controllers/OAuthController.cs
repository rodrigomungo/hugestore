﻿using System;
using System.Configuration;
using System.Web.Mvc;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using SL.Hugestore.Domain.MercadoLibre.Response;
using SL.Hugestore.Services.Extensions;
using SL.Hugestore.Services.MercadoLibreClient;
using SL.Hugestore.Services.MercadoLibreClient.Exceptions;

namespace HugestoreWebAPI.Controllers
{
    public class OAuthController : Controller
    {
        // GET: OAuth
        public ActionResult Index(string code)
        {
            long aplicacionId = long.Parse(ConfigurationManager.AppSettings["MercadoLibreApplicationId"]);
            Empresa empresa = EmpresaService.Current.ObtenerUno(aplicacionId);
            MercadoLibreManager _meliClient = new MercadoLibreManager(empresa.AplicacionId, empresa.ClaveSecreta, "https://localhost:44358/OAuth");

            string result = String.Empty;
            string detail = String.Empty;
            try
            {
                if (!code.IsNullOrEmpty())
                {
                    OAuthTokenResponse token = _meliClient.Authorize(code);
                    empresa.Token = token.AccessToken;
                    empresa.TokenRefresco = token.RefreshToken;
                    empresa.Expiracion = token.ExpiresIn;
                    empresa.FechaAutenticacion = DateTimeOffset.UtcNow;
                    EmpresaService.Current.Actualizar(empresa);
                    result = "Se autenticó correctamente. Ya puede cerrar esta ventana";
                }
            }
            catch (MercadoLibreClientException ex)
            {
                result = "No se pudo autenticar con MercadoLibre";
                detail = ex.HttpError.Message;
            }
            catch (Exception ex)
            {
                result = "No se pudo autenticar con MercadoLibre";
                detail = ex.Message;
            }
            

            ViewBag.Result = result;
            ViewBag.Detail = detail;
            return View();
        }
    }
}