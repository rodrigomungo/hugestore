﻿namespace SL.Hugestore.DAL.Contracts
{
    internal interface IAdapter<T>
    {
        T Adapt(object[] values);
    }
}
