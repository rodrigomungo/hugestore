﻿using System.Collections.Generic;

namespace SL.Hugestore.DAL.Contracts
{
    public interface IMaintenance
    {
        void Restore(string database, string bkpPath);
        void Backup(string database, string path);
        IEnumerable<string> GetDatabases();
    }
}
