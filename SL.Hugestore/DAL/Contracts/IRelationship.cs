﻿using System;
using System.Collections.Generic;

namespace SL.Hugestore.DAL.Contracts
{
    internal interface IRelationship<T, U>
    {
        void Join(T parent, U nested);
        List<U> Get(T parent);
    }
}
