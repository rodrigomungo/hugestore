﻿using System;
using System.Collections.Generic;

namespace SL.Hugestore.DAL.Contracts
{
    internal interface IRepository<T, U>
    {
        void Insert(T entity);
        void Delete(U id);
        void Update(T entity);
        T GetOne(U id);
        IEnumerable<T> GetAll();
    }
}
