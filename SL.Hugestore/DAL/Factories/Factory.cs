﻿using System;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Repositories.SqlServer;
using SL.Hugestore.DAL.Repositories.SqlServer.Maintenance;
using SL.Hugestore.Domain;
using SL.Hugestore.Domain.SecurityComposite;

namespace SL.Hugestore.DAL.Factories
{
    internal sealed class Factory
    {
        #region Singleton
        private readonly static Factory _instance = new Factory();

        public static Factory Current
        {
            get
            {
                return _instance;
            }
        }

        private Factory()
        {
            GrupoRepository = new GrupoRepository();
            RolRepository = new RolRepository();
            UsuarioRepository = new UsuarioRepository();
            GrupoGrupoRelationship = new GrupoGrupoRelationship();
            GrupoRolRelationship = new GrupoRolRelationship();
            UsuarioGrupoRelationship = new UsuarioGrupoRelationship();
            UsuarioRolRelationship = new UsuarioRolRelationship();
            LogRepository = new LogRepository();
            MaintenanceRepository = new DbMaintenance();
        }
        #endregion

        public IRepository<Grupo, Guid> GrupoRepository { get; set; }
        public IRepository<Rol, Guid> RolRepository { get; set; }
        public IRepository<Usuario, Usuario> UsuarioRepository { get; set; }
        public IRelationship<Grupo, Grupo> GrupoGrupoRelationship { get; set; }
        public IRelationship<Grupo, Rol> GrupoRolRelationship { get; set; }
        public IRelationship<Usuario, Grupo> UsuarioGrupoRelationship { get; set; }
        public IRelationship<Usuario, Rol> UsuarioRolRelationship { get; set; }
        public IRepository<Log, Guid> LogRepository { get; set; }
        public IMaintenance MaintenanceRepository { get; set; }
    }
}
