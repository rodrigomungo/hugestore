﻿using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Repositories.SqlServer.Maintenance;

namespace SL.Hugestore.DAL.Factories
{
    public sealed class MaintenanceFactory
    {
        #region Singleton
        private readonly static MaintenanceFactory _instance = new MaintenanceFactory();

        public static MaintenanceFactory Current
        {
            get
            {
                return _instance;
            }
        }

        private MaintenanceFactory()
        {
            MaintenanceRepository = new DbMaintenance();
        }
        #endregion

        public IMaintenance MaintenanceRepository { get; set; }
    }
}
