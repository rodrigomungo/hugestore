﻿using System;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.Domain.SecurityComposite;

namespace SL.Hugestore.DAL.Repositories.SqlServer.Adapters
{
    public sealed class GrupoAdapter : IAdapter<Grupo>
    {
        #region Singleton
        private readonly static GrupoAdapter _instance = new GrupoAdapter();

        public static GrupoAdapter Current
        {
            get => _instance;
        }

        private GrupoAdapter() { }
        #endregion

        public Grupo Adapt(object[] values) => new Grupo()
        {
            Id = Guid.Parse(values[0].ToString()),
            Nombre = values[1].ToString()
        };
    }
}
