﻿using System;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.Domain.SecurityComposite;

namespace SL.Hugestore.DAL.Repositories.SqlServer.Adapters
{
    public sealed class RolAdapter : IAdapter<Rol>
    {
        #region Singleton
        private readonly static RolAdapter _instance = new RolAdapter();

        public static RolAdapter Current
        {
            get => _instance;
        }

        private RolAdapter() { }
        #endregion

        public Rol Adapt(object[] values) => new Rol()
        {
            Id = Guid.Parse(values[0].ToString()),
            Nombre = values[1].ToString(),
            Vista = values[2].ToString()
        };
    }
}
