﻿using System;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.Domain.SecurityComposite;

namespace SL.Hugestore.DAL.Repositories.SqlServer.Adapters
{
    public sealed class UsuarioAdapter : IAdapter<Usuario>
    {
        #region Singleton
        private readonly static UsuarioAdapter _instance = new UsuarioAdapter();

        public static UsuarioAdapter Current
        {
            get => _instance;
        }

        private UsuarioAdapter() { }
        #endregion

        public Usuario Adapt(object[] values) => new Usuario()
        {
            Id = Guid.Parse(values[0].ToString()),
            Nombre = values[1].ToString(),
            Apellido = values[2].ToString(),
            NombreCompleto = values[3].ToString(),
            NombreUsuario = values[4].ToString()
        };
    }
}
