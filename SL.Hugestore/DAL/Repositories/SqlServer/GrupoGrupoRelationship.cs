﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    internal class GrupoGrupoRelationship : IRelationship<Grupo, Grupo>
    {
        #region Statements
        private string SelectRelationshipStatement
        {
            get => "SELECT GrupoId, GrupoHijoId FROM [dbo].[GrupoGrupos] WHERE GrupoId = @GrupoId";
        }

        private string InsertRelationshipStatement
        {
            get => "INSERT [dbo].[GrupoGrupos] (GrupoId, GrupoHijoId) VALUES (@GrupoId, @GrupoHijoId)";
        }
        #endregion

        public List<Grupo> Get(Grupo parent)
        {
            List<Grupo> grupos = new List<Grupo>();
            IRepository<Grupo, Guid> grupoRepository = new GrupoRepository();
            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectRelationshipStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@GrupoId", parent.Id)))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        Guid childGroupId = Guid.Parse(values[1].ToString());
                        Grupo grupo = grupoRepository.GetOne(childGroupId);
                        grupos.Add(grupo);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return grupos;
        }

        public void Join(Grupo parent, Grupo nested)
        {
            try
            {
                SqlHelper.ExecuteScalar(
                    InsertRelationshipStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@GrupoId", parent.Id),
                    new SqlParameter("@GrupoHijoId", nested.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
