﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Repositories.SqlServer.Adapters;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    internal class GrupoRepository : IRepository<Grupo, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Grupos] (Nombre) OUTPUT Inserted.Id VALUES (@Nombre)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Grupos] SET Nombre = @Nombre WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "DELETE FROM [dbo].[Grupos] WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Nombre FROM [dbo].[Grupos] WHERE Id = @Id";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Nombre FROM [dbo].[Grupos]";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public IEnumerable<Grupo> GetAll()
        {
            List<Grupo> grupos = new List<Grupo>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        grupos.Add(GrupoAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return grupos;
        }

        public Grupo GetOne(Guid id)
        {
            Grupo grupo = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(SelectOneStatement, System.Data.CommandType.Text, new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        grupo = GrupoAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return grupo;
        }

        public void Insert(Grupo entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public void Update(Grupo entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
