﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    internal class GrupoRolRelationship : IRelationship<Grupo, Rol>
    {
        #region Statements
        private string SelectRelationshipStatement
        {
            get => "SELECT GrupoId, RolId FROM [dbo].[GrupoRoles] WHERE GrupoId = @GrupoId";
        }

        private string InsertRelationshipStatement
        {
            get => "INSERT [dbo].[GrupoRoles] (GrupoId, RolId) VALUES (@GrupoId, @RolId)";
        }
        #endregion

        public List<Rol> Get(Grupo parent)
        {
            List<Rol> roles = new List<Rol>();
            IRepository<Rol, Guid> rolRepository = new RolRepository();
            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectRelationshipStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@GrupoId", parent.Id)))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        Guid roleId = Guid.Parse(values[1].ToString());
                        Rol rol = rolRepository.GetOne(roleId);
                        roles.Add(rol);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return roles;
        }

        public void Join(Grupo parent, Rol nested)
        {
            try
            {
                SqlHelper.ExecuteScalar(
                    InsertRelationshipStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@GrupoId", parent.Id),
                    new SqlParameter("@RolId", nested.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
