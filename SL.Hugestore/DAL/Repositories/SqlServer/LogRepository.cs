﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    public class LogRepository : IRepository<Log, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Logs] (Mensaje, Level, Ensamblado) OUTPUT Inserted.Id VALUES (@Mensaje, @Level, @Ensamblado)";
        }
        #endregion

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Log> GetAll()
        {
            throw new NotImplementedException();
        }

        public Log GetOne(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Insert(Log entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Mensaje", entity.Mensaje),
                    new SqlParameter("@Level", entity.Level),
                    new SqlParameter("@Ensamblado", entity.Ensamblado));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Log entity)
        {
            throw new NotImplementedException();
        }
    }
}
