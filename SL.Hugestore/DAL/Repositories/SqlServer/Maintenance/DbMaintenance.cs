﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer.Maintenance
{
    internal sealed class DbMaintenance : IMaintenance
    {
        #region Statements
        private string SelectAllStatement
        {
            get => "SELECT name FROM sys.databases WHERE database_id > 4";
        }

        private string BackupStatement
        {
            get => "USE master BACKUP DATABASE [{0}] TO DISK=@FileName";
        }

        private string RestoreStatement
        {
            get => "USE master RESTORE DATABASE [{0}] FROM DISK=@FileName";
        }
        #endregion

        public void Backup(string database, string path)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    string.Format(BackupStatement, database),
                    System.Data.CommandType.Text,
                    new SqlParameter("@FileName", path));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public void Restore(string database, string bkpPath)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    string.Format(RestoreStatement, database),
                    System.Data.CommandType.Text,
                    new SqlParameter("@FileName", bkpPath));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public IEnumerable<string> GetDatabases()
        {
            List<string> databaseNames = new List<string>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        databaseNames.Add(values[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return databaseNames;
        }
    }
}
