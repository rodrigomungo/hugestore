﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Repositories.SqlServer.Adapters;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    internal class RolRepository : IRepository<Rol, Guid>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Roles] (Nombre, NombreFormulario) OUTPUT Inserted.Id VALUES (@Nombre, @NombreFormulario)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Roles] SET Nombre = @Nombre, NombreFormulario = @NombreFormulario WHERE Id = @Id";
        }

        private string DeleteStatement
        {
            get => "DELETE FROM [dbo].[Roles] WHERE Id = @Id";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Nombre, NombreFormulario FROM [dbo].[Roles] WHERE Id = @Id";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Nombre, NombreFormulario FROM [dbo].[Roles]";
        }
        #endregion

        public void Delete(Guid id)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Id", id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public IEnumerable<Rol> GetAll()
        {
            List<Rol> roles = new List<Rol>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        roles.Add(RolAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return roles;
        }

        public Rol GetOne(Guid id)
        {
            Rol rol = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(SelectOneStatement, System.Data.CommandType.Text, new SqlParameter("@Id", id)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        rol = RolAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return rol;
        }

        public void Insert(Rol entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@NombreFormulario", entity.Vista));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public void Update(Rol entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@NombreFormulario", entity.Vista));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
