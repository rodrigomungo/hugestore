﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    internal class UsuarioGrupoRelationship : IRelationship<Usuario, Grupo>
    {
        #region Statements
        private string SelectRelationshipStatement
        {
            get => "SELECT UsuarioId, GrupoId FROM [dbo].[UsuarioGrupos] WHERE UsuarioId = @UsuarioId";
        }

        private string InsertRelationshipStatement
        {
            get => "INSERT [dbo].[UsuarioGrupos] (UsuarioId, GrupoId) VALUES (@UsuarioId, @GrupoId)";
        }
        #endregion

        public List<Grupo> Get(Usuario parent)
        {
            List<Grupo> grupos = new List<Grupo>();
            IRepository<Grupo, Guid> grupoRepository = new GrupoRepository();
            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectRelationshipStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@UsuarioId", parent.Id)))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        Guid groupId = Guid.Parse(values[1].ToString());
                        Grupo grupo = grupoRepository.GetOne(groupId);
                        grupos.Add(grupo);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return grupos;
        }

        public void Join(Usuario parent, Grupo nested)
        {
            try
            {
                SqlHelper.ExecuteScalar(
                    InsertRelationshipStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@UsuarioId", parent.Id),
                    new SqlParameter("@GrupoId", nested.Id));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
