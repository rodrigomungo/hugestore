﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Contracts;
using SL.Hugestore.DAL.Repositories.SqlServer.Adapters;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain.SecurityComposite;
using SL.Hugestore.Services;

namespace SL.Hugestore.DAL.Repositories.SqlServer
{
    internal class UsuarioRepository : IRepository<Usuario, Usuario>
    {
        #region Statements
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Usuarios] (Nombre, Apellido, NombreUsuario, Clave) OUTPUT Inserted.Id VALUES (@Nombre, @Apellido, @NombreUsuario, @Clave)";
        }

        private string UpdateStatement
        {
            get => "UPDATE [dbo].[Usuarios] SET Nombre = @Nombre, Apellido = @Apellido, Clave = @Clave WHERE NombreUsuario = @NombreUsuario";
        }

        private string DeleteStatement
        {
            get => "UPDATE [dbo].[Usuarios] SET FechaCierre = GETDATE() WHERE NombreUsuario = @NombreUsuario";
        }

        private string SelectOneStatement
        {
            get => "SELECT Id, Nombre, Apellido, NombreCompleto, NombreUsuario FROM [dbo].[Usuarios] WHERE NombreUsuario = @NombreUsuario AND Clave = @Clave AND FechaCierre IS NULL";
        }

        private string SelectAllStatement
        {
            get => "SELECT Id, Nombre, Apellido, NombreCompleto, NombreUsuario FROM [dbo].[Usuarios] WHERE FechaCierre IS NULL";
        }
        #endregion

        public void Delete(Usuario entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    DeleteStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@NombreUsuario", entity.NombreUsuario));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public IEnumerable<Usuario> GetAll()
        {
            List<Usuario> usuarios = new List<Usuario>();

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectAllStatement,
                    System.Data.CommandType.Text))
                {
                    while (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        usuarios.Add(UsuarioAdapter.Current.Adapt(values));
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return usuarios;
        }

        public Usuario GetOne(Usuario entity)
        {
            Usuario usuario = default;

            try
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(
                    SelectOneStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@NombreUsuario", entity.NombreUsuario),
                    new SqlParameter("@Clave", entity.Clave)))
                {
                    if (dr.Read())
                    {
                        object[] values = new object[dr.FieldCount];
                        dr.GetValues(values);
                        usuario = UsuarioAdapter.Current.Adapt(values);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }

            return usuario;
        }

        public void Insert(Usuario entity)
        {
            try
            {
                entity.Id = (Guid)SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Apellido", entity.Apellido),
                    new SqlParameter("@NombreUsuario", entity.NombreUsuario),
                    new SqlParameter("@Clave", entity.Clave));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }

        public void Update(Usuario entity)
        {
            try
            {
                SqlHelper.ExecuteNonQuery(
                    UpdateStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Nombre", entity.Nombre),
                    new SqlParameter("@Apellido", entity.Apellido),
                    new SqlParameter("@Clave", entity.Clave),
                    new SqlParameter("@NombreUsuario", entity.NombreUsuario));
            }
            catch (Exception ex)
            {
                ExceptionManager.Current.Handle(this, ex);
            }
        }
    }
}
