﻿namespace SL.Hugestore.Domain
{
    public class CloudinaryAsset
    {
        public string Url { get; set; }
        public string SecureUrl { get; set; }
    }
}
