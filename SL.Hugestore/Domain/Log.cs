﻿using System;
using System.Diagnostics.Tracing;

namespace SL.Hugestore.Domain
{
    public class Log
    {
        public Guid Id { get; set; }

        public DateTime Fecha { get; set; }

        public string Mensaje { get; set; }

        public EventLevel Level { get; set; }

        public string Ensamblado { get; set; }
    }
}
