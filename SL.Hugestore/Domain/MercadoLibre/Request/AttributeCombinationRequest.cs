﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class AttributeCombinationRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value_id")]
        public string ValueId { get; set; }

        [JsonProperty("value_name")]
        public string ValueName { get; set; }
    }
}
