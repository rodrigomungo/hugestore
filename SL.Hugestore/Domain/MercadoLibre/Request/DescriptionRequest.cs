﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class DescriptionRequest
    {
        [JsonProperty("plain_text")]
        public string PlainText { get; set; }
    }
}
