﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class ImageRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
