﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class ItemImageRequest
    {
        [JsonProperty("source")]
        public string Source { get; set; }
    }
}
