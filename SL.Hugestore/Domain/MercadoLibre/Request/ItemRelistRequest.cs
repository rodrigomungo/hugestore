using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
  public class ItemRelistRequest
  {
    [JsonProperty("price")]
    public decimal Price { get; set; }

    [JsonProperty("quantity")]
    public int quantity { get; set; }

    [JsonProperty("listing_type_id")]
    public string ListingTypeId { get; set; }

    [JsonProperty("variations", NullValueHandling = NullValueHandling.Ignore)]
    public List<object> Variations { get; set; }
  }
}