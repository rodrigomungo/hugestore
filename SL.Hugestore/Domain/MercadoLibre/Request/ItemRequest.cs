﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class ItemRequest
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("category_id")]
        public string CategoriId { get; set; }

        [JsonProperty("site_id")]
        public string SiteId { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("currency_id")]
        public string CurrencyId { get; set; }

        [JsonProperty("available_quantity")]
        public int AvailableQuantity { get; set; } = 1;

        [JsonProperty("official_store_id")]
        public long? OfficialStoreId { get; set; }

        [JsonProperty("buying_mode")]
        public string BuyingMode { get; set; }

        [JsonProperty("condition")]
        public string Condition { get; set; }

        [JsonProperty("listing_type_id")]
        public string ListingTypeId { get; set; }

        [JsonProperty("sale_terms")]
        public List<KeyValueRequest> SaleTerms { get; set; }

        [JsonProperty("pictures")]
        public List<ItemImageRequest> Pictures { get; set; }

        [JsonProperty("attributes")]
        public List<KeyValueRequest> Attributes { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("variations")]
        public List<VariationRequest> Variations { get; set; }
    }
}
