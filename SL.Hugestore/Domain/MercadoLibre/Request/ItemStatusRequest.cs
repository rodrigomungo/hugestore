using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
  public class ItemStatusRequest
  {

    [JsonProperty("status")]
    public string Status { get; set; }

  }
}