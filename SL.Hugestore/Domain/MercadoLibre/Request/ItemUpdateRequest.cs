using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
  public class ItemUpdateRequest
  {
    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("price")]
    public decimal Price { get; set; }

    [JsonProperty("available_quantity")]
    public int AvailableQuantity { get; set; }

  }
}