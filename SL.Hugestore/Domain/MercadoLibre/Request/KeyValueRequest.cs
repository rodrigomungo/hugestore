﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class KeyValueRequest
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("value_name")]
        public string ValueName { get; set; }
    }
}
