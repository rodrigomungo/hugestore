﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class NotificationRequest
    {
        [JsonProperty("resource")]
        public string Resource { get; set; }

        [JsonProperty("user_id")]
        public long UserId { get; set; }

        [JsonProperty("topic")]
        public string Topic { get; set; }

        [JsonProperty("application_id")]
        public long ApplicationId { get; set; }

        [JsonProperty("attempts")]
        public int Attempts { get; set; }

        [JsonProperty("sent")]
        public DateTime Sent { get; set; }

        [JsonProperty("received")]
        public DateTime Received { get; set; }
    }
}
