﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Request
{
    public class VariationRequest
    {
        [JsonProperty("attribute_combinations")]
        public List<AttributeCombinationRequest> AttributeCombinations { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("available_quantity")]
        public int AvailableQuantity { get; set; }

        [JsonProperty("attributes")]
        public List<KeyValueRequest> Attributes { get; set; }

        [JsonProperty("picture_ids")]
        public List<string> PictureIds { get; set; }
    }
}
