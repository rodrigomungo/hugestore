﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class AttributeResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value_type")]
        public string ValueType { get; set; }

        [JsonProperty("value_max_length", NullValueHandling = NullValueHandling.Ignore)]
        public int ValueMaxLength { get; set; }

        [JsonProperty("values")]
        public List<AttributeValueResponse> Values { get; set; }

        [JsonProperty("attribute_group_id")]
        public string AttributeGroupId { get; set; }

        [JsonProperty("attribute_group_name")]
        public string AttributeGroupName { get; set; }
    }
}
