﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class BillingAdditionalInfoResponse
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
