﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class BillingInfoResponse
    {
        [JsonProperty("additional_info")]
        public List<BillingAdditionalInfoResponse> AdditionalInfo { get; set; }

        [JsonProperty("doc_number")]
        public string DocNumber { get; set; }

        [JsonProperty("doc_type")]
        public string DocType { get; set; }
    }
}
