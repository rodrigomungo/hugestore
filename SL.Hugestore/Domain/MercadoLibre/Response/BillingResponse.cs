﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class BillingResponse
    {
        [JsonProperty("billing_info")]
        public BillingInfoResponse BillingInfo { get; set; }
    }
}
