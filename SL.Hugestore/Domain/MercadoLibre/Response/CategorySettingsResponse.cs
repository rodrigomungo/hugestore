﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class CategorySettingsResponse
    {
        [JsonProperty("buying_modes")]
        public List<string> BuyingModes { get; set; }

        [JsonProperty("currencies")]
        public List<string> Currencies { get; set; }

        [JsonProperty("item_conditions")]
        public List<string> ItemConditions { get; set; }

        [JsonProperty("max_pictures_per_item")]
        public int MaxPicturesPerItem { get; set; }

        [JsonProperty("max_description_length")]
        public int MaxDescriptionLength { get; set; }

        [JsonProperty("max_title_length")]
        public int MaxTitleLength { get; set; }

        [JsonProperty("shipping_modes")]
        public List<string> ShippingModes { get; set; }

        [JsonProperty("shipping_options")]
        public List<string> ShippingOptions { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
