﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class DescriptionResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
