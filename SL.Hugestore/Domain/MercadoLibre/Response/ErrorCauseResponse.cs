﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ErrorCauseResponse
    {
        [JsonProperty("department")]
        public string Department { get; set; }

        [JsonProperty("cause_id")]
        public int CauseId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("references")]
        public List<string> References { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
