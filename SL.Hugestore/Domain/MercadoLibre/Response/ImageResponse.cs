﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ImageResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
