﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ItemAttributeCombinationResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value_id")]
        public string ValueId { get; set; }

        [JsonProperty("value_name")]
        public string ValueName { get; set; }

        [JsonProperty("value_struct")]
        public Dictionary<string, object> ValueStruct { get; set; }

        [JsonProperty("values")]
        public List<ItemAttributeCombinationValueResponse> Values { get; set; }
    }
}
