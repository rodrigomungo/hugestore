﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ItemImageResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("secure_url")]
        public string SecureUrl { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }

        [JsonProperty("max_size")]
        public string MaxSize { get; set; }

        [JsonProperty("quality")]
        public string Quality { get; set; }
    }
}
