﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ItemResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("site_id")]
        public string SiteId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("seller_id")]
        public long SellerId { get; set; }

        [JsonProperty("category_id")]
        public string CategoriId { get; set; }

        [JsonProperty("official_store_id")]
        public long? OfficialStoreId { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("base_price")]
        public decimal BasePrice { get; set; }

        [JsonProperty("original_price")]
        public decimal OriginalPrice { get; set; }

        [JsonProperty("currency_id")]
        public string CurrencyId { get; set; }

        [JsonProperty("initial_quantity")]
        public int InitialQuantity { get; set; }

        [JsonProperty("available_quantity")]
        public int AvailableQuantity { get; set; }

        [JsonProperty("sold_quantity")]
        public int SoldQuantity { get; set; }

        [JsonProperty("buying_mode")]
        public string BuyingMode { get; set; }

        [JsonProperty("listing_type_id")]
        public string ListingTypeId { get; set; }

        [JsonProperty("start_time")]
        public DateTimeOffset StartTime { get; set; }

        [JsonProperty("stop_time")]
        public DateTimeOffset StopTime { get; set; }

        [JsonProperty("end_time")]
        public DateTimeOffset EndTime { get; set; }

        [JsonProperty("expiration_time")]
        public DateTimeOffset ExpirationTime { get; set; }

        [JsonProperty("condition")]
        public string Condition { get; set; }

        [JsonProperty("permalink")]
        public string Permalink { get; set; }

        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }

        [JsonProperty("secure_thumbnail")]
        public string SecureThumbnail { get; set; }

        [JsonProperty("pictures")]
        public List<ItemImageResponse> Pictures { get; set; }

        [JsonProperty("video_id")]
        public string VideoId { get; set; }

        [JsonProperty("sale_terms")]
        public List<SaleTermResponse> SaleTerms { get; set; }

        [JsonProperty("descriptions")]
        public List<DescriptionResponse> Descriptions { get; set; }

        [JsonProperty("accepts_mercadopago")]
        public bool AcceptsMercadoPago { get; set; }

        [JsonProperty("shipping")]
        public ItemShippingResponse Shipping { get; set; }

        [JsonProperty("attributes")]
        public List<ItemAttributeResponse> Attributes { get; set; }

        [JsonProperty("variations")]
        public List<ItemVariationResponse> Variations { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("sub_status")]
        public List<string> SubStatus { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("catalog_product_id")]
        public long? CatalogProductId { get; set; }

        [JsonProperty("channels")]
        public List<string> Channels { get; set; }
    }
}
