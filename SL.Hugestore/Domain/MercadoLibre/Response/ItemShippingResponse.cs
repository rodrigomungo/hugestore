﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ItemShippingResponse
    {
        [JsonProperty("mode")]
        public string Mode { get; set; }

        [JsonProperty("methods")]
        public List<string> Methods { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("dimensions")]
        public object Dimensions { get; set; }

        [JsonProperty("local_pick_up")]
        public bool LocalPickUp { get; set; }

        [JsonProperty("free_shipping")]
        public bool FreeShipping { get; set; }

        [JsonProperty("logistic_type")]
        public string LogisticType { get; set; }

        [JsonProperty("store_pick_up")]
        public bool StorePickUp { get; set; }
    }
}
