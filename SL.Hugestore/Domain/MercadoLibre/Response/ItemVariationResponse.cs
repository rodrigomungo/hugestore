﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ItemVariationResponse
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("attribute_combinations")]
        public List<ItemAttributeCombinationResponse> AttributeCombinations { get; set; }

        [JsonProperty("available_quantity")]
        public int AvailableQuantity { get; set; }

        [JsonProperty("sold_quantity")]
        public int SoldQuantity { get; set; }

        [JsonProperty("sale_terms")]
        public List<SaleTermResponse> SaleTerms { get; set; }

        [JsonProperty("picture_ids")]
        public List<string> PictureIds { get; set; }

        [JsonProperty("catalog_product_id")]
        public long? CatalogProductId { get; set; }
    }
}
