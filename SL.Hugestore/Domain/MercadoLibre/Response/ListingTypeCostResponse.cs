using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ListingTypeCostResponse
    {
        [JsonProperty("listing_type_id")]
        public string ListingTypeId { get; set; }

        [JsonProperty("listing_type_name")]
        public string ListingTypeName { get; set; }

        [JsonProperty("listing_exposure")]
        public string ListingExposure { get; set; }

        [JsonProperty("requires_picture")]
        public bool RequiresPicture { get; set; }

        [JsonProperty("currency_id")]
        public string CurrencyId { get; set; }

        [JsonProperty("listing_fee_amount")]
        public decimal ListingFeeAmount { get; set; }

        [JsonProperty("sale_fee_amount")]
        public decimal SaleFeeAmount { get; set; }

        [JsonProperty("free_relist")]
        public bool FreeRelist { get; set; }

        [JsonProperty("stop_time")]
        public DateTimeOffset StopTime { get; set; }
    }
}