﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class ListingTypeResponse
    {
        [JsonProperty("site_id")]
        public string SiteId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
