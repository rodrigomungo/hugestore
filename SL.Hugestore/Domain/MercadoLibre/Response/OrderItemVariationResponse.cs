﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class OrderItemVariationResponse
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("attribute_combinations")]
        public List<OrderItemVariationAttributeResponse> AttributeCombinations { get; set; }
    }
}
