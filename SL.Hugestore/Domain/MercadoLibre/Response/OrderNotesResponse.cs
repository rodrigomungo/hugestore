﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class OrderNotesResponse
    {
        [JsonProperty("results")]
        public List<OrderNoteResponse> Results { get; set; }

        [JsonProperty("order_id")]
        public long OrderId { get; set; }
    }
}
