﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class OrderReceiverAddressResponse
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("address_line")]
        public string AddressLine { get; set; }

        [JsonProperty("street_name")]
        public string StreetName { get; set; }

        [JsonProperty("street_number")]
        public string StreetNumber { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("zip_code")]
        public string ZipCode { get; set; }

        [JsonProperty("latitude")]
        public decimal? Latitude { get; set; }

        [JsonProperty("longitude")]
        public decimal? Longitude { get; set; }

        [JsonProperty("receiver_name")]
        public string ReceiverName { get; set; }

        [JsonProperty("receiver_phone")]
        public string ReceiverPhone { get; set; }

        [JsonProperty("city")]
        public OrderReceiverAddressAdditionalResponse City { get; set; }

        [JsonProperty("state")]
        public OrderReceiverAddressAdditionalResponse State { get; set; }

        [JsonProperty("country")]
        public OrderReceiverAddressAdditionalResponse Country { get; set; }

        [JsonProperty("neighborhood")]
        public OrderReceiverAddressAdditionalResponse Neighborhood { get; set; }
    }
}
