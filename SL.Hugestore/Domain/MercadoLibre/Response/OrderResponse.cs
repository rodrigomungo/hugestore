﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class OrderResponse
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("order_items")]
        public List<OrderItemResponse> OrderItems { get; set; }

        [JsonProperty("date_created")]
        public DateTimeOffset DateCreated { get; set; }

        [JsonProperty("date_closed")]
        public DateTimeOffset DateClosed { get; set; }

        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        [JsonProperty("paid_amount")]
        public decimal PaidAmount { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("payments")]
        public List<OrderPaymentResponse> Payments { get; set; }

        [JsonProperty("buyer")]
        public List<OrderUserResponse> Buyer { get; set; }

        [JsonProperty("pack_id")]
        public long? PackId { get; set; }

        [JsonProperty("shipping")]
        public List<OrderShippingResponse> Shipping { get; set; }

        [JsonProperty("taxes")]
        public List<OrderTaxesResponse> Taxes { get; set; }
    }
}
