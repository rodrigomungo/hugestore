﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class OrderTaxesResponse
    {
        [JsonProperty("amount")]
        public decimal? Amount { get; set; }
    }
}
