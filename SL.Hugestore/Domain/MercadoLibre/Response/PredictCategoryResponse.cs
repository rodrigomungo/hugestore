﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain.MercadoLibre.Response
{
    public class PredictCategoryResponse
    {
        [JsonProperty("domain_id")]
        public string DomainId { get; set; }

        [JsonProperty("domain_name")]
        public string DomainName { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("attributes")]
        public List<PredictCategoryAttributeResponse> Attributes { get; set; }
    }
}
