﻿using System;

namespace SL.Hugestore.Domain.SecurityComposite
{
    public abstract class Componente
    {
        public Guid Id { get; set; }
        public string Nombre { get; set; }
        public abstract int CantidadComponentes { get; }

        public Componente() { }

        public abstract void Add(Componente componente);
        public abstract void Remove(Componente componente);
    }
}
