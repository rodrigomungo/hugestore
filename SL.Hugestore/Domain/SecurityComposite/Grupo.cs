﻿using System;
using System.Collections.Generic;

namespace SL.Hugestore.Domain.SecurityComposite
{
    public class Grupo : Componente
    {
		private List<Componente> componentes = new List<Componente>();

		public List<Componente> Componentes => componentes;

		public override int CantidadComponentes => componentes.Count;

		public Grupo() { }

		public Grupo(Componente componente)
		{
			componentes.Add(componente);
		}

		public override void Add(Componente componente)
		{
			componentes.Add(componente);
		}

		public override void Remove(Componente componente)
		{
			// TODO: Verificar que exista un grupo antes.
			componentes.Remove(componente);
		}
	}
}
