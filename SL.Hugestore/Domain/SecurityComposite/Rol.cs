﻿using System;
using SL.Hugestore.Services.Extensions;

namespace SL.Hugestore.Domain.SecurityComposite
{
    public class Rol : Componente
    {
        public override int CantidadComponentes => 0;

        public string Vista { get; set; }
        
        public Rol() { }

        public override void Add(Componente component)
        {
            throw new Exception("Cannot add items in a role".Traducir());
        }

        public override void Remove(Componente component)
        {
            throw new Exception("Cannot remove items in a role".Traducir());
        }
    }
}
