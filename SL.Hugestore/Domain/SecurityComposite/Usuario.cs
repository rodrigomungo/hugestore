﻿using System;
using System.Collections.Generic;
using System.Linq;
using SL.Hugestore.Services.Session;

namespace SL.Hugestore.Domain.SecurityComposite
{
    public class Usuario : IUsuario
    {
        public Guid Id { get; set; }

        public string CodigoCultura { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string NombreCompleto { get; set; }

        public string NombreUsuario { get; set; }

        public string Clave { get; set; }

        public List<Componente> Permisos { get; set; } = new List<Componente>();

        public List<Rol> PermisosMenu
        {
            get
            {
                List<Rol> roles = new List<Rol>();
                CargarRoles(Permisos, roles);
                return roles;
            }
        }

        public List<Grupo> Roles
        {
            get
            {
                List<Grupo> grupos = new List<Grupo>();
                CargarGrupos(Permisos, grupos);
                return grupos;
            }
        }

        public bool TieneRol(string roleName)
        {
            return PermisosMenu.Any(p => p.Nombre.Equals(roleName));
        }

        private void CargarRoles(List<Componente> permisos, List<Rol> roles)
        {
            foreach (var permisoItem in permisos)
            {
                if (permisoItem.CantidadComponentes == 0)
                {
                    roles.Add(permisoItem as Rol);
                }
                if (permisoItem.CantidadComponentes > 0)
                {
                    CargarRoles((permisoItem as Grupo).Componentes, roles);
                }
            }
        }

        private void CargarGrupos(List<Componente> permisos, List<Grupo> grupos)
        {
            foreach (var permisoItem in permisos)
            {
                if (permisoItem.CantidadComponentes > 0)
                {
                    grupos.Add(permisoItem as Grupo);
                    CargarGrupos((permisoItem as Grupo).Componentes, grupos);
                }
            }
        }
    }
}
