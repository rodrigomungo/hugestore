﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain
{
    public class SendGridDynamicTemplateData
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }
    }
}
