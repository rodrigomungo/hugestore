﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain
{
    public class SendGridEmailAddress
    {
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
