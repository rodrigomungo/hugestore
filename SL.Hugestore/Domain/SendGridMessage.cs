﻿using System;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain
{
    public class SendGridMessage
    {
        [JsonProperty("from")]
        public SendGridEmailAddress From { get; set; }

        [JsonProperty("personalizations")]
        public SendGridPersonalizations Personalizations { get; set; }

        [JsonProperty("template_id")]
        public string TemplateId { get; set; }
    }
}
