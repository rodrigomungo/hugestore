﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SL.Hugestore.Domain
{
    public class SendGridPersonalizations
    {
        [JsonProperty("to")]
        public List<SendGridEmailAddress> To { get; set; }

        [JsonProperty("dynamic_template_data")]
        public SendGridDynamicTemplateData DynamicTemplateData { get; set; }
    }
}
