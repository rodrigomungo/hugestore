﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using SL.Hugestore.Domain;

namespace SL.Hugestore.Services.CloudinaryClient
{
    public sealed class CloudinaryManager
    {
        private Cloudinary _cloudinaryClient;
        public CloudinaryManager() { }

        private void _connect()
        {
            string appName = ConfigurationManager.AppSettings["CloudinaryAppName"].ToString();
            string appId = ConfigurationManager.AppSettings["CloudinaryAppId"].ToString();
            string secretKey = ConfigurationManager.AppSettings["CloudinarySecretKey"].ToString();
            Account account = new Account(appName, appId, secretKey);
            _cloudinaryClient = new Cloudinary(account);
        }

        private List<CloudinaryAsset> ListByPrefix(string prefix)
        {
            var listResourcesByPrefixParams = new ListResourcesByPrefixParams()
            {
                Type = "upload",
                Prefix = prefix,
            };
            ListResourcesResult results = _cloudinaryClient.ListResources(listResourcesByPrefixParams);
            return results.Resources.Select(r => new CloudinaryAsset()
            {
                Url = r.Url.AbsoluteUri,
                SecureUrl = r.SecureUrl.AbsoluteUri
            }).ToList();
        }

        public CloudinaryAsset Upload(string path, string publicId, string folder)
        {
            _connect();
            ImageUploadParams uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(path),
                PublicId = publicId,
                Overwrite = true,
                Folder = folder
            };
            ImageUploadResult uploadResult = _cloudinaryClient.Upload(uploadParams);
            CloudinaryAsset asset = new CloudinaryAsset()
            {
                Url = uploadResult.Url.AbsoluteUri,
                SecureUrl = uploadResult.SecureUrl.AbsoluteUri
            };

            return asset;
        }
    }
}
