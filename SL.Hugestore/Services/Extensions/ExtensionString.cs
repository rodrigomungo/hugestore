﻿namespace SL.Hugestore.Services.Extensions
{
    public static class ExtensionString
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string Traducir(this string word)
        {
            return I18nManager.Current.Translate(word);
        }

        public static bool IsNullOrEmpty(this string word)
        {
            return string.IsNullOrEmpty(word);
        }
    }
}
