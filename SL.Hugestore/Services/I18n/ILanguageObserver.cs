﻿using System.Globalization;

namespace SL.Hugestore.Services.I18n
{
    public interface ILanguageObserver
    {
        void UpdateLanguage(CultureInfo cultura);
    }
}
