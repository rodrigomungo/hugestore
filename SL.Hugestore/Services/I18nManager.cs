﻿using System.IO;
using System.Threading;

namespace SL.Hugestore.Services
{
    internal class I18nManager
    {
        private string BasePath = @"I18n\language.";
        #region Singleton
        private readonly static I18nManager _instance = new I18nManager();

        public static I18nManager Current
        {
            get
            {
                return _instance;
            }
        }

        private I18nManager() { }
        #endregion

        public string Translate(string key)
        {
            string translated = key;
            string culture = Thread.CurrentThread.CurrentUICulture.Name;
            using (StreamReader streamReader = new StreamReader($"{BasePath}{culture}"))
            {
                while (!streamReader.EndOfStream)
                {
                    string linea = streamReader.ReadLine();
                    string[] keyValuePair = linea.Split(':');
                    if (keyValuePair[0].ToLower() == key.ToLower())
                    {
                        translated = keyValuePair[1];
                        break;
                    }
                }
            }

            return translated;
        }
    }
}
