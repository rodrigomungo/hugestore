﻿using SL.Hugestore.Services.LogChainOfResponsivity.Contracts;

namespace SL.Hugestore.Services.LogChainOfResponsivity
{
    public abstract class AbstractHandler : IHandler
    {
        private IHandler _nextHandler;

        public IHandler SetNext(IHandler handler)
        {
            this._nextHandler = handler;
            return handler;
        }

        public virtual void Handle(object request)
        {
            if (_nextHandler != null)
            {
                _nextHandler.Handle(request);
            }
        }
    }
}
