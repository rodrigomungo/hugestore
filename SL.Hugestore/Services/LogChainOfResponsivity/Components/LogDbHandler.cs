﻿using System;
using System.Data.SqlClient;
using SL.Hugestore.DAL.Tools;
using SL.Hugestore.Domain;

namespace SL.Hugestore.Services.LogChainOfResponsivity.Components
{
    public class LogDbHandler : AbstractHandler
    {
        private string InsertStatement
        {
            get => "INSERT INTO [dbo].[Roles] (Nombre, NombreFormulario) OUTPUT Inserted.Id VALUES (@Nombre, @NombreFormulario)";
        }

        public override void Handle(object request)
        {
            Log log = request as Log;
            try
            {
                SqlHelper.ExecuteScalar(
                    InsertStatement,
                    System.Data.CommandType.Text,
                    new SqlParameter("@Mensaje", log.Mensaje),
                    new SqlParameter("@Level", log.Level),
                    new SqlParameter("@Ensamblado", log.Ensamblado));
            }
            catch (Exception)
            {
                base.Handle(request);
            }
        }
    }
}
