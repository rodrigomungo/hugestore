﻿using System;
using System.Collections.Generic;
using System.Configuration;
using RestSharp;
using SL.Hugestore.Domain;

namespace SL.Hugestore.Services.LogChainOfResponsivity.Components
{
    public class LogEmailHandler : AbstractHandler
    {
        private readonly IRestClient _client;
        private string _apiKey = ConfigurationManager.AppSettings["SendGridApiKey"];
        private string _templateId = ConfigurationManager.AppSettings["SendGridTemplateId"];

        public LogEmailHandler()
        {
            _client = new RestClient("https://api.sendgrid.com/v3");
        }

        public override void Handle(object request)
        {
            Log log = request as Log;
            SendGridMessage body = new SendGridMessage()
            {
                From = new SendGridEmailAddress()
                {
                    Email = "rodrigomungo@gmail.com"
                },
                Personalizations = new SendGridPersonalizations()
                {
                    To = new List<SendGridEmailAddress>()
                    {
                        new SendGridEmailAddress() { Email = "rodrigomungo@gmail.com" }
                    },
                    DynamicTemplateData = new SendGridDynamicTemplateData()
                    {
                        Message = log.Mensaje,
                        Level = log.Level.ToString()
                    }
                },
                TemplateId = _templateId
            };
            try
            {
                IRestRequest req = new RestRequest("/mail/send");
                req.AddHeader("Accept", "application/json");
                req.AddHeader("Authorization", $"Bearer {_apiKey}");
                req.AddJsonBody(body);
                _client.Post(req);
            }
            catch (Exception)
            {
                base.Handle(request);
            }
        }
    }
}
