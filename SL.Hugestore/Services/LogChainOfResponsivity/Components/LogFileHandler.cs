﻿using System;
using log4net;
using SL.Hugestore.Domain;

namespace SL.Hugestore.Services.LogChainOfResponsivity.Components
{
    public class LogFileHandler : AbstractHandler
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void Handle(object request)
        {
            Log log = request as Log;
            try
            {
                logger.Error($"{DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") } [Severity { log.Level }] : { log.Mensaje }");
            }
            catch(Exception)
            {
                base.Handle(request);
            }
        }
    }
}
