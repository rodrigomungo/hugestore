﻿namespace SL.Hugestore.Services.LogChainOfResponsivity.Contracts
{
    public interface IHandler
    {
        IHandler SetNext(IHandler handler);

        void Handle(object request);
    }
}
