﻿using System;
using System.Configuration;
using System.Diagnostics.Tracing;
using SL.Hugestore.BLL.Exceptions;
using SL.Hugestore.Domain;
using SL.Hugestore.Services.Extensions;
using SL.Hugestore.Services.LogChainOfResponsivity.Components;
using SL.Hugestore.Services.LogChainOfResponsivity.Contracts;

namespace SL.Hugestore.Services
{
    public sealed class ExceptionManager
    {
        private string DAL_ASSEMBLY_NAME = ConfigurationManager.AppSettings["DALComponent"];
        private string BLL_ASSEMBLY_NAME = ConfigurationManager.AppSettings["BLLComponent"];
        private string UI_ASSEMBLY_NAME = ConfigurationManager.AppSettings["UIComponent"];
        private string SL_ASSEMBLY_NAME = ConfigurationManager.AppSettings["SLComponent"];

        private IHandler logDb = new LogDbHandler();
        private IHandler logFile = new LogFileHandler();
        private IHandler logEmail = new LogEmailHandler();

        #region Singleton
        private readonly static ExceptionManager _instance = new ExceptionManager();

        public static ExceptionManager Current
        {
            get
            {
                return _instance;
            }
        }

        private ExceptionManager()
        {
            logDb.SetNext(logFile).SetNext(logEmail);
        }
        #endregion

        public void Handle(object sender, Exception ex)
        {
            string assemblyName = sender.GetType().Assembly.GetName().Name;
            if (assemblyName == UI_ASSEMBLY_NAME)
            {
                HandleUI(ex);
            }
            else if (assemblyName == BLL_ASSEMBLY_NAME)
            {
                HandleBLL(ex);
            }
            else if (assemblyName == DAL_ASSEMBLY_NAME)
            {
                HandleDAL(ex);
            }
            else if (assemblyName == SL_ASSEMBLY_NAME)
            {
                HandleSL(ex);
            }
            else
            {
                throw new Exception("No se conoce el source para gestión de errores");
            }
        }

        private void HandleUI(Exception ex)
        {
            Log log = new Log()
            {
                Mensaje = $"[Mensaje: {ex.Message}][StackTrace: {ex.StackTrace}]",
                Level = EventLevel.Informational,
                Ensamblado = UI_ASSEMBLY_NAME
            };
            logDb.Handle(log);
        }

        private void HandleBLL(Exception ex)
        {
            if (ex.InnerException != null)
            {
                throw new Exception("Can't access the DB server at this time".Traducir(), ex);
            }
            else
            {
                if (ex is BusinessException)
                {
                    Log log = new Log()
                    {
                        Mensaje = $"[Mensaje: {ex.Message}][StackTrace: {ex.StackTrace}]",
                        Level = EventLevel.Warning,
                        Ensamblado = BLL_ASSEMBLY_NAME
                    };
                    logDb.Handle(log);
                    throw ex;
                }
                else
                {
                    Log log = new Log()
                    {
                        Mensaje = $"[Mensaje: {ex.Message}][StackTrace: {ex.StackTrace}]",
                        Level = EventLevel.Warning,
                        Ensamblado = BLL_ASSEMBLY_NAME
                    };
                    logDb.Handle(log);
                    throw new Exception("An unexpected error has occurred, please try again later".Traducir(), ex);
                }
            }
        }

        private void HandleDAL(Exception ex)
        {
            Log log = new Log()
            {
                Mensaje = $"[Mensaje: {ex.Message}][StackTrace: {ex.StackTrace}]",
                Level = EventLevel.Error,
                Ensamblado = DAL_ASSEMBLY_NAME
            };
            logDb.Handle(log);
            throw new Exception("DAL Exception", ex);
        }

        private void HandleSL(Exception ex)
        {
            Log log = new Log()
            {
                Mensaje = $"[Mensaje: {ex.Message}][StackTrace: {ex.StackTrace}]",
                Level = EventLevel.Error,
                Ensamblado = SL_ASSEMBLY_NAME
            };
            logDb.Handle(log);
            throw new Exception("SL Exception", ex);
        }
    }
}
