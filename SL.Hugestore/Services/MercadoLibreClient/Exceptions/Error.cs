using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace SL.Hugestore.Services.MercadoLibreClient.Exceptions
{
  public class Error
  {

    [JsonProperty("message")]
    public string Message { get; set; }

    [JsonProperty("error")]
    public string Error { get; set; }

    [JsonProperty("status")]
    public HttpStatusCode Status { get; set; }

    [JsonProperty("cause")]
    public List<string> Cause { get; set; }

  }
}