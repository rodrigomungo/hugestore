using System;
using SL.Hugestore.Domain.MercadoLibre.Response;

namespace SL.Hugestore.Services.MercadoLibreClient.Exceptions
{
    public class MercadoLibreClientException : Exception
    {
        private ErrorResponse _httpError;

        public ErrorResponse HttpError
        {
            get => _httpError;
        }

        public MercadoLibreClientException(string message, ErrorResponse error) : base(message)
        {
            _httpError = error;
        }

        public MercadoLibreClientException(string message, Exception innerEx, ErrorResponse error) : base(message, innerEx)
        {
            _httpError = error;
        }
    }
}