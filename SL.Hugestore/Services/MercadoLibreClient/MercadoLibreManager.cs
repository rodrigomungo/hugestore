﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using SL.Hugestore.Domain.MercadoLibre.Request;
using SL.Hugestore.Domain.MercadoLibre.Response;
using SL.Hugestore.Services.MercadoLibreClient.Exceptions;


namespace SL.Hugestore.Services.MercadoLibreClient
{
    /// <summary>
    /// Clase <c>MercadoLibreManager</c> Interactúa con el API pública de MercadoLibre.
    /// </summary>
    public sealed class MercadoLibreManager
    {
        const string AUTHURL = "https://auth.mercadolibre.com.ar";

        private readonly IRestClient _client;
        private string _secretKey;
        private long _clientId;
        private string _redirectUri;

        public MercadoLibreManager(long clientId, string secretKey, string redirectUri)
        {
            _clientId = clientId;
            _secretKey = secretKey;
            _redirectUri = redirectUri;

            _client = new RestClient("https://api.mercadolibre.com");
            _client.UserAgent = "MELI-NET-SDK-1.0.2";
        }

        /// <summary>
        /// Método <c>GetAuthUrl</c> obtiene la URL para iniciar el proceso de autorización de usuario.
        /// </summary>
        /// <returns>URL de autorización</returns>
        public string GetAuthUrl()
        {
            return $"{AUTHURL}/authorization?response_type=code&client_id={_clientId}";
        }

        /// <summary>
        /// Método <c>Authorize</c> autentica con sus credenciales en MercadoLibre para obtener un token de acceso a los recursos privados del API.
        /// </summary>
        /// <param name="code">Código de autenticación obtenido luego de iniciar sesión en la plataforma de MercadoLibre</param>
        /// <returns>Token de acceso para los recursos privados</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public OAuthTokenResponse Authorize(string code)
        {
            IRestRequest request = new RestRequest($"/oauth/token?grant_type=authorization_code&client_id={_clientId}&client_secret={_secretKey}&code={code}&redirect_uri={_redirectUri}");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = _client.Post(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<OAuthTokenResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>RefreshToken</c> renueva las credenciales de acceso al API de MercadoLibre.
        /// </summary>
        /// <param name="refreshToken">Token de refresco para renovar las credenciales</param>
        /// <returns>Token de acceso para los recursos privados</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public OAuthTokenResponse RefreshToken(string refreshToken)
        {
            IRestRequest request = new RestRequest($"/oauth/token?grant_type=refresh_token&client_id={_clientId}&client_secret={_secretKey}&refresh_token={refreshToken}");
            request.AddHeader("Accept", "application/json");
            IRestResponse response = _client.Post(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<OAuthTokenResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>Publish</c> publica un producto en el sitio.
        /// </summary>
        /// <param name="body">Objeto que contiene toda la información para publicar un producto</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Item publicado en la plataforma</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public ItemResponse Publish(ItemRequest body, string accessToken)
        {
            IRestRequest request = new RestRequest("/items");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Post(request);
            if (response.StatusCode.Equals(HttpStatusCode.Created))
            {
                return JsonConvert.DeserializeObject<ItemResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>AddDescription</c> agrega una descripción a la publicación.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="body">Objeto que contiene toda la información para añadir una descripción a la publicación</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void AddDescription(string itemId, DescriptionRequest body, string accessToken)
        {
            IRestRequest request = new RestRequest($"/items/{itemId}/descriptions");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Post(request);
            if (!response.StatusCode.Equals(HttpStatusCode.Created))
            {
                ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                throw new MercadoLibreClientException("MercadoLibre HttpError", error);
            }
        }

        /// <summary>
        /// Método <c>GetItem</c> publica un producto en el sitio.
        /// </summary>
        /// <param name="id">Id del item publicado</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Item publicado en la plataforma</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public ItemResponse GetItem(string id, string accessToken)
        {
            IRestRequest request = new RestRequest($"/items/{id}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<ItemResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetListingTypes</c> obtiene los tipos de listas en donde se puede publicar el producto.
        /// </summary>
        /// <param name="siteId">Id del sitio que indica la plataforma utilizada</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Listado de tipos de lista</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public List<ListingTypeResponse> GetListingTypes(string siteId, string accessToken)
        {
            IRestRequest request = new RestRequest($"/sites/{siteId}/listing_types");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<List<ListingTypeResponse>>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetCategory</c> obtiene los datos de la categoria.
        /// </summary>
        /// <param name="id">Id de la categoría</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Datos de la categoría</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public CategoryResponse GetCategory(string id, string accessToken)
        {
            IRestRequest request = new RestRequest($"/categories/{id}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<CategoryResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>PredictCategory</c> predice la categoría correspondiente según el título del producto.
        /// </summary>
        /// <param name="siteId">Id del sitio que indica la plataforma utilizada</param>
        /// <param name="title">Título del producto</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Datos de la categoría</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public List<PredictCategoryResponse> PredictCategory(string siteId, string title, string accessToken)
        {
            if (title.Length.Equals(0))
            {
                throw new Exception("MercadoLibre error: The field <title> is required");
            }

            IRestRequest request = new RestRequest($"/sites/{siteId}/domain_discovery/search?q={title}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<List<PredictCategoryResponse>>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetAttributes</c> obtiene los atributos de una categoría.
        /// </summary>
        /// <param name="categoryId">Id de la categoría</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Atributos de una categoría</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public List<AttributeResponse> GetAttributes(string categoryId, string accessToken)
        {
            IRestRequest request = new RestRequest($"/categories/{categoryId}/attributes");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<List<AttributeResponse>>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetVariation</c> obtiene una variación de un item publicado.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="variationId">Id de la variación del item</param>
        /// <returns>Variación de un item publicado</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public OrderItemVariationResponse GetVariation(string itemId, long variationId)
        {
            IRestRequest request = new RestRequest($"/items/{itemId}/variations/{variationId}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<OrderItemVariationResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>UploadImage</c> sube una imagen a la plataforma.
        /// </summary>
        /// <param name="fileName">Nombre del archivo</param>
        /// <param name="path">Ruta completa del archivo</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Obtiene el Id de la imagen publicada</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public ImageResponse UploadImage(string fileName, string path, string accessToken)
        {
            var request = new RestRequest("/pictures/items/upload");
            request.AlwaysMultipartFormData = true;
            request.AddFile($"files[{fileName}]", path);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Post(request);
            if (response.StatusCode.Equals(HttpStatusCode.Created))
            {
                return JsonConvert.DeserializeObject<ImageResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>AttachImage</c> agrega una imagen a un item publicado.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="body">Contiene la información necesaria para agregar una imagen</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void AttachImage(string itemId, ItemImageRequest body, string accessToken)
        {
            IRestRequest request = new RestRequest($"/items/{itemId}/pictures");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Post(request);
            if (!response.StatusCode.Equals(HttpStatusCode.OK))
            {
                ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                throw new MercadoLibreClientException("MercadoLibre HttpError", error);
            }
        }

        /// <summary>
        /// Método <c>CloseItem</c> marca un item publicado como cerrado.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void CloseItem(string itemId, string accessToken)
        {
            changeItemStatus(itemId, "closed", accessToken);
        }

        /// <summary>
        /// Método <c>PauseItem</c> marca un item publicado como pausado.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void PauseItem(string itemId, string accessToken)
        {
            changeItemStatus(itemId, "paused", accessToken);
        }

        /// <summary>
        /// Método <c>ActiveItem</c> marca un item publicado como activo.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void ActiveItem(string itemId, string accessToken)
        {
            changeItemStatus(itemId, "active", accessToken);
        }

        private void changeItemStatus(string itemId, string status, string accessToken)
        {
            ItemStatusRequest body = new ItemStatusRequest();
            body.Status = status;

            IRestRequest request = new RestRequest($"/items/{itemId}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Put(request);
            if (!response.StatusCode.Equals(HttpStatusCode.NoContent))
            {
                ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                throw new MercadoLibreClientException("MercadoLibre HttpError", error);
            }
        }

        /// <summary>
        /// Método <c>ReListItem</c> republica un item con datos modificados.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="body">Datos del item a republicar</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Item republicado</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public ItemResponse ReListItem(string itemId, ItemRelistRequest body, string accessToken)
        {
            IRestRequest request = new RestRequest($"/items/{itemId}/relist");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Post(request);
            if (response.StatusCode.Equals(HttpStatusCode.Created))
            {
                return JsonConvert.DeserializeObject<ItemResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>UpdateItem</c> actualiza un item publicado. El título solo se puede modificar si el item no tiene ventas (sold_quantity)
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="body">Datos del item a republicar</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void UpdateItem(string itemId, ItemUpdateRequest body, string accessToken)
        {
            IRestRequest request = new RestRequest($"/items/{itemId}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Put(request);
            if (!response.StatusCode.Equals(HttpStatusCode.OK))
            {
                ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                throw new MercadoLibreClientException("MercadoLibre HttpError", error);
            }
        }

        /// <summary>
        /// Método <c>UpdateItemDescription</c> actualiza la descripción del item publicado.
        /// </summary>
        /// <param name="itemId">Id del item publicado</param>
        /// <param name="body">Descripción del item</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public void UpdateItemDescription(string itemId, DescriptionRequest body, string accessToken)
        {
            IRestRequest request = new RestRequest($"/items/{itemId}/description?api_version=2");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            request.AddJsonBody(body);
            IRestResponse response = _client.Put(request);
            if (!response.StatusCode.Equals(HttpStatusCode.OK))
            {
                ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                throw new MercadoLibreClientException("MercadoLibre HttpError", error);
            }
        }

        /// <summary>
        /// Método <c>ListingTypeCostResponse</c> obtiene el costo del tipo de lista.
        /// </summary>
        /// <param name="siteId">Id del sitio</param>
        /// <param name="listingTypeId">Id del tipo de lista</param>
        /// <param name="price">Valor monetario del item que se quiere publicar</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Costo del tipo de lista</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public ListingTypeCostResponse GetCostByListingType(string siteId, string listingTypeId, decimal price, string accessToken)
        {
            IRestRequest request = new RestRequest($"/sites/{siteId}/listing_prices?price={price}&listing_type_id={listingTypeId}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Post(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<ListingTypeCostResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetOrder</c> obtiene una orden por Id.
        /// </summary>
        /// <param name="orderId">Id de la orden</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Datos de la orden</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public OrderResponse GetOrder(string orderId, string accessToken)
        {
            IRestRequest request = new RestRequest($"/orders/{orderId}");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("x-format-new", "true");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK) || response.StatusCode.Equals(HttpStatusCode.PartialContent))
            {
                return JsonConvert.DeserializeObject<OrderResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetOrderNotes</c> obtiene las notas adjuntas a la orden.
        /// </summary>
        /// <param name="orderId">Id de la orden</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Listado de notas adjuntas a la orden</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public List<OrderNoteResponse> GetOrderNotes(string orderId, string accessToken)
        {
            IRestRequest request = new RestRequest($"/orders/{orderId}/notes");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                List<OrderNotesResponse> result = JsonConvert.DeserializeObject<List<OrderNotesResponse>>(response.Content);
                return result[0].Results;
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetShipping</c> obtiene un envío por Id.
        /// </summary>
        /// <param name="shippingId">Id del envío</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Datos del envío</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public OrderShippingResponse GetShipping(string shippingId, string accessToken)
        {
            IRestRequest request = new RestRequest($"/shipments/{shippingId}");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK) || response.StatusCode.Equals(HttpStatusCode.PartialContent))
            {
                return JsonConvert.DeserializeObject<OrderShippingResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetShippingLabelsAsPDF</c> obtiene las etiquetas de los envíos en formato PDF.
        /// </summary>
        /// <param name="shippingIds">Ids de envíos</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>MemoryStream que contiene los datos del archivo PDF</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public MemoryStream GetShippingLabelsAsPDF(string[] shippingIds, string accessToken)
        {
            string ids = string.Join(",", shippingIds);
            IRestRequest request = new RestRequest($"/shipment_labels?shipment_ids={ids}&response_type=pdf");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return new MemoryStream(response.RawBytes);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }

        /// <summary>
        /// Método <c>GetBillingData</c> obtiene los datos de facturación de la orden.
        /// </summary>
        /// <param name="orderId">Id de la orden</param>
        /// <param name="accessToken">Token de acceso para validar que es un usuario autenticado</param>
        /// <returns>Datos de facturación</returns>
        /// <exception cref="MercadoLibreClientException">
        /// Si el API devuelve un código de error se genera una excepción del tipo <c>MercadoLibreClienteException</c>.
        /// </exception>
        public BillingResponse GetBillingData(long orderId, string accessToken)
        {
            IRestRequest request = new RestRequest($"/orders/{orderId}/billing_info");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", $"Bearer {accessToken}");
            IRestResponse response = _client.Get(request);
            if (response.StatusCode.Equals(HttpStatusCode.OK))
            {
                return JsonConvert.DeserializeObject<BillingResponse>(response.Content);
            }

            ErrorResponse error = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
            throw new MercadoLibreClientException("MercadoLibre HttpError", error);
        }
    }
}
