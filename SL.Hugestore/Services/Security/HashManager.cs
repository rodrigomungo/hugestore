﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SL.Hugestore.Services.Security
{
    public static class HashManager
    {
        private const string SALT = "UserName";

        public static string Hash(string word)
        {
            HashAlgorithm hash = new SHA256Managed();
            // Compute hash of the password prefixing password with the salt
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(SALT + word);
            byte[] hashBytes = hash.ComputeHash(plainTextBytes);

            return Convert.ToBase64String(hashBytes);
        }
    }
}
