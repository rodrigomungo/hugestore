﻿using System;
using System.Collections.Generic;
using System.Linq;
using SL.Hugestore.DAL.Factories;
using SL.Hugestore.Domain.SecurityComposite;

namespace SL.Hugestore.Services.Security
{
    public sealed class UserFacade
    {
        #region Singleton
        private readonly static UserFacade _instance = new UserFacade();

        public static UserFacade Current
        {
            get
            {
                return _instance;
            }
        }

        private UserFacade() { }
        #endregion

        #region Usuarios
        public Usuario LogIn(Usuario usuario)
        {
            usuario.Clave = HashManager.Hash(usuario.Clave);
            Usuario usuarioEncontrado = Factory.Current.UsuarioRepository.GetOne(usuario);
            if (usuarioEncontrado != null)
            {
                List<Rol> roles = Factory.Current.UsuarioRolRelationship.Get(usuarioEncontrado);
                foreach (Rol rol in roles)
                {
                    usuarioEncontrado.Permisos.Add(rol);
                }

                List<Grupo> grupos = Factory.Current.UsuarioGrupoRelationship.Get(usuarioEncontrado);
                foreach (Grupo grupo in grupos)
                {
                    usuarioEncontrado.Permisos.Add(grupo);
                }

                Scratch(grupos);
            }
            return usuarioEncontrado;
        }

        private void Scratch(List<Grupo> grupos)
        {
            foreach(Grupo grupo in grupos)
            {
                List<Rol> roles = Factory.Current.GrupoRolRelationship.Get(grupo);
                foreach (Rol rol in roles)
                {
                    grupo.Add(rol);
                }

                List<Grupo> gruposAnidados = Factory.Current.GrupoGrupoRelationship.Get(grupo);
                foreach(Grupo grupoAnidado in gruposAnidados)
                {
                    grupo.Add(grupoAnidado);
                }

                Scratch(gruposAnidados);
            }
        }

        public void Register(Usuario usuario)
        {
            usuario.Clave = HashManager.Hash(usuario.Clave);
            Factory.Current.UsuarioRepository.Insert(usuario);
        }

        public Usuario GetOneUser(Usuario usuario)
        {
            return Factory.Current.UsuarioRepository.GetOne(usuario);
        }

        public void CloseUser()
        {

        }
        #endregion

        #region Roles
        public void CreateRole(Rol rol)
        {
            Factory.Current.RolRepository.Insert(rol);
        }

        public Rol GetRole(Guid id)
        {
            return Factory.Current.RolRepository.GetOne(id);
        }

        public List<Rol> GetRoles()
        {
            return Factory.Current.RolRepository.GetAll().ToList();
        }

        public void DeleteRole(Guid id)
        {
            Factory.Current.RolRepository.Delete(id);
        }
        #endregion

        #region Grupos
        public void CreateGroup(Grupo grupo)
        {
            Factory.Current.GrupoRepository.Insert(grupo);
        }

        public Grupo GetGroup(Guid id)
        {
            return Factory.Current.GrupoRepository.GetOne(id);
        }

        public List<Grupo> GetGroups()
        {
            return Factory.Current.GrupoRepository.GetAll().ToList();
        }

        public void DeleteGroup(Guid id)
        {
            Factory.Current.GrupoRepository.Delete(id);
        }
        #endregion
    }
}
