﻿namespace SL.Hugestore.Services.Session
{
    public interface IUsuario
    {
        string CodigoCultura { get; set; }
        string NombreUsuario { get; set; }
        string Clave { get; set; }
    }
}
