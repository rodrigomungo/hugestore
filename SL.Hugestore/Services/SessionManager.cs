﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using SL.Hugestore.Services.I18n;
using SL.Hugestore.Services.Session;

namespace SL.Hugestore.Services
{
    public static class SessionManager
    {
        static IUsuario _session;

        static IList<ILanguageObserver> _observers = new List<ILanguageObserver>();
        public static IUsuario Session
        {
            get
            {
                return _session;
            }
        }

        public static bool IsLogged()
        {
            return _session != null;
        }

        public static void Login(IUsuario usuario)
        {
            _session = usuario;
        }

        public static void Logout()
        {
            _session = null;
            Notificar(Thread.CurrentThread.CurrentCulture);
        }

        public static void Suscribir(ILanguageObserver subscriptor)
        {
            _observers.Add(subscriptor);
        }
        public static void Desuscribir(ILanguageObserver subscriptor)
        {
            _observers.Remove(subscriptor);
        }

        private static void Notificar(CultureInfo culture)
        {
            foreach (var observer in _observers)
            {
                observer.UpdateLanguage(culture);
            }
        }

        public static void CambiarIdioma(CultureInfo culture)
        {
            Thread.CurrentThread.CurrentUICulture = culture;
            if (_session != null)
            {
                _session.CodigoCultura = culture.Name;
                Notificar(culture);
            }
        }
    }
}
