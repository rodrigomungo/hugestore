﻿using System;
using System.Configuration;
using BLL.Hugestore.Services;
using Domain.Hugestore;
using SL.Hugestore.Domain.MercadoLibre.Response;
using SL.Hugestore.Services;
using SL.Hugestore.Services.MercadoLibreClient;

namespace ScripTokenRefresh.Hugestore.Controllers
{
    public sealed class TokenController
    {
        #region singleton
        private readonly static TokenController _instance = new TokenController();

        public static TokenController Current
        {
            get
            {
                return _instance;
            }
        }

        private TokenController() { }
        #endregion

        public void RefreshToken()
        {
            Console.WriteLine("****************** INICIANDO PROCESO ******************");

            Console.WriteLine("***************** OBTENIENDO VARIABLES ****************");
            long aplicacionId;
            long.TryParse(ConfigurationManager.AppSettings["MercadoLibreApplicationId"], out aplicacionId);
            string redirectUri = ConfigurationManager.AppSettings["MercadoLibreRedirectUri"];

            try
            {
                Console.WriteLine("***************** GENERANDO SERVICIOS *****************");
                Empresa empresa = EmpresaService.Current.ObtenerUno(aplicacionId);
                MercadoLibreManager _meliClient = new MercadoLibreManager(empresa.AplicacionId, empresa.ClaveSecreta, redirectUri);

                Console.WriteLine("*****************  REFRESCANDO TOKEN  *****************");
                OAuthTokenResponse token = _meliClient.RefreshToken(empresa.TokenRefresco);
                empresa.Token = token.AccessToken;
                empresa.TokenRefresco = token.RefreshToken;
                empresa.FechaAutenticacion = DateTimeOffset.UtcNow;
                empresa.Expiracion = token.ExpiresIn;
                EmpresaService.Current.Actualizar(empresa);
            }
            catch (Exception ex)
            {
                Console.WriteLine("**************** ERROR: REVISE LOS LOGS ***************");
                ExceptionManager.Current.Handle(this, ex);
            }

            Console.WriteLine("******************  FIN DEL PROCESO  ******************");
        }
    }
}
