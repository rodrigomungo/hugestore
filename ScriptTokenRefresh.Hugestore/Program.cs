﻿using System;
using ScripTokenRefresh.Hugestore.Controllers;

namespace ScriptTokenRefresh.Hugestore
{
    class Program
    {
        static void Main(string[] args)
        {
            TokenController.Current.RefreshToken();
        }
    }
}
